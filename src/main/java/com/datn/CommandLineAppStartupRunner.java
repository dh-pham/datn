package com.datn;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.websocket.EndpointConfig;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.datn.repository.auth.AppRoleRepository;
import com.datn.service.auth.IUserDetailsService;
import com.datn.service.classifier.ClassifierService;
import com.datn.service.seafood.ISeaFoodService;
import com.datn.service.symptom.ISymptomService;
import com.datn.service.symptom.ISymptomTypeService;
import com.datn.utils.OtherUtil;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
	@Autowired
	private ISymptomTypeService symptomTypeService;
	
	@Autowired
	private ClassifierService classifierService;
	
	@Autowired
	private IUserDetailsService iUserDetailsService;
	
	@Autowired
	private ISeaFoodService seaFoodService;
	
	@Autowired
	private AppRoleRepository roleRepo;
	
	@Autowired
	private ISymptomService symptomService;

	private static final Logger logger = LoggerFactory.getLogger(CommandLineRunner.class);

	@Override
	public void run(String... args) throws Exception {
		classifierService.loadDataOrCreateIfNotExisted();
//		File file = new File("./src/main/resources/static/img/profile/default.png");
//		byte[] bytes = Files.readAllBytes(file.toPath());
//		String encodedString = Base64.getEncoder().encodeToString(bytes);
//		System.out.println(encodedString);
//		byte[] imageBytes = Base64.getDecoder().decode(encodedString);
//		ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes);
//		BufferedImage imageBuffer = ImageIO.read(bis);
//		bis.close();
//		File outputFile = new File("./src/main/resources/static/image/default.png");
//		outputFile.createNewFile();
//		ImageIO.write(imageBuffer, "png", outputFile);

	}
	
	
}