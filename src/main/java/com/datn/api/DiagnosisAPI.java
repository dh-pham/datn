package com.datn.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.diasease.Diasease;
import com.datn.entity.symptom.SymptomType;
import com.datn.entity.symptom.SymptomValue;
import com.datn.service.classifier.ClassifierService;
import com.datn.service.diasease.IDiaseaseService;
import com.datn.service.seafood.ISeaFoodService;
import com.datn.service.symptom.ISymptomTypeService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.minidev.json.JSONObject;

@RestController
@RequestMapping("/api")
public class DiagnosisAPI {
	
	@Autowired
	private ISeaFoodService seaFoodService;
	
	@Autowired
	private ClassifierService classifierService;
	
	@Autowired
	private ISymptomTypeService symptomTypeService;
	
	@Autowired
	private IDiaseaseService diaseaseService;

	private static final Logger logger = LoggerFactory.getLogger(DiagnosisAPI.class);

	@GetMapping("/symptomTypesAndInner")
	public ResponseEntity<String> getAllSymptomTypeAndInner(
			@RequestParam(value = "seaFoodId", defaultValue = "-1") String seaFoodIdStr) {
		Integer seaFoodId = Integer.valueOf(seaFoodIdStr);
		List<SymptomType> symptomTypes = symptomTypeService.getAll();
		List<JSONObject> jsonResult = symptomTypes.stream().map(symptomTypeEle -> {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("id", symptomTypeEle.getId());
			jsonObj.put("symptomTypeName", symptomTypeEle.getSymptomTypeName());
			List<JSONObject> symptoms = symptomTypeEle.getSymptoms().stream().map(p -> {
				JSONObject symptomJson = new JSONObject();
				symptomJson.put("id", p.getId());
				symptomJson.put("symptomName", p.getSymptomName());
				symptomJson.put("symptomQuestion", p.getSymptomQuestion());
				List<SymptomValue> symptomValues;
				if (seaFoodId == -1) {
					symptomValues = p.getSymptomValues();
				} else {
					symptomValues = getSymptomValues(seaFoodId, p.getId());
				}
				
				List<JSONObject> symptomValuesJsonList = new ArrayList<JSONObject>();
				for (SymptomValue symptomValueEle : symptomValues) {
					JSONObject symptomValueJson = new JSONObject();
					symptomValueJson.put("id", symptomValueEle.getId());
					symptomValueJson.put("symptomValueDesc", symptomValueEle.getSymptomValueDesc());
					symptomValueJson.put("indexNumber", symptomValueEle.getIndexNumber());
					symptomValuesJsonList.add(symptomValueJson);
				}
				symptomJson.put("symptomValues", symptomValuesJsonList);
				return symptomJson;
			}).collect(Collectors.toList());
			jsonObj.put("symptoms", symptoms);
			return jsonObj;
		}).collect(Collectors.toList());
		return new ResponseEntity<String>(jsonResult.toString(), HttpStatus.OK);
	}
	
	
	
	private List<SymptomValue> getSymptomValues(Integer seaFoodId, Integer symptomId) {
		return seaFoodService.getSymptomValues(seaFoodId, symptomId);
	}



//	@RequestMapping(value="/getDiaseaseFromSymptoms", method = RequestMethod.POST)
//	public ResponseEntity<String> getDiagnosisFromSymptoms(@RequestBody String seafoodAndSymptoms) {
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			JsonNode jsonNode = mapper.readTree(seafoodAndSymptoms);
//			Integer seaFoodId = jsonNode.get("seafood_id").asInt();
//			JsonNode symptomsNode = jsonNode.get("symptoms");
//			Map<Integer, String> symptoms = new HashMap<Integer, String>();
//			for (JsonNode symptomNode : symptomsNode) {
//				symptoms.put(symptomNode.get("symptom_id").asInt(), 
//						symptomNode.get("symptom_value").asText());
//			}
//			List<String> symptomValues = new ArrayList<String>();
//			symptomValues = symptoms.entrySet().stream().sorted(Map.Entry.comparingByKey())
//				.map(ele -> ele.getValue())
//				.collect(Collectors.toList());
//			String diaseaseResult = classifierService.predict(seaFoodId, symptomValues);
//			if (diaseaseResult == null) {
//				return new ResponseEntity<String>("Don't have any data about this diaseaseId: " + diaseaseResult, HttpStatus.NOT_FOUND);
//			}
//			int diaseaseIdResult = Integer.parseInt(diaseaseResult);
//			Optional<Diasease> diaseaseOptional = diaseaseService.getById(diaseaseIdResult);
//			
//			if(diaseaseOptional.isPresent()) {
//				Diasease diasease = diaseaseOptional.get();
//				String responseStr = mapper.writeValueAsString(diasease);
//				return new ResponseEntity<String>(responseStr, HttpStatus.OK);
//			} else {
//				return new ResponseEntity<String>("Diasease Id not found!", HttpStatus.NOT_FOUND);
//			}
//			
//		} catch (IOException e) {
////			e.printStackTrace();
//			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
	
	@RequestMapping(value="/getTopResultList", method = RequestMethod.POST)
	public ResponseEntity<String> getTopResultWithPercentage(@RequestBody String seafoodAndSymptoms) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(seafoodAndSymptoms);
			Integer seaFoodId = jsonNode.get("seafood_id").asInt();
			JsonNode symptomsNode = jsonNode.get("symptoms");
			Map<Integer, String> symptoms = new HashMap<Integer, String>();
			for (JsonNode symptomNode : symptomsNode) {
				symptoms.put(symptomNode.get("symptom_id").asInt(), 
						symptomNode.get("symptom_value").asText());
			}
			List<String> symptomValues = new ArrayList<String>();
			symptomValues = symptoms.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.map(ele -> ele.getValue())
				.collect(Collectors.toList());
			Map<String, Double> topResult = classifierService
					.getTopResultWithPercentage(5, seaFoodId, symptomValues);
			List<String> keys = topResult.keySet().stream().collect(Collectors.toList());
			Collections.sort(keys, (o1, o2) -> topResult.get(o2).compareTo(topResult.get(o1)));
			
			ArrayNode resultArr = mapper.createArrayNode();
			
			for (String key : keys) {
				ObjectNode resultObj = mapper.createObjectNode();
				Integer diaseaseId = Integer.valueOf(key);
				Double percentage = topResult.get(key);
				Optional<Diasease> diaseaseOpt = diaseaseService.getById(diaseaseId);
				if (diaseaseOpt.isPresent()) {
					Diasease diasease = diaseaseOpt.get();
					JsonNode diaseaseNode = mapper.convertValue(diasease, JsonNode.class);
					resultObj.set("diaseaseData", diaseaseNode);
					resultObj.put("percentage", percentage);
					resultArr.add(resultObj);
				}
			}
			return new ResponseEntity<String>(mapper.writeValueAsString(resultArr)
					, HttpStatus.OK);
		} catch (IOException e) {
			return new ResponseEntity<String>("error", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value = "/diagnosis/suggestion/{seaFoodId}")
	public ResponseEntity<String> getSuggestion(@PathVariable(name = "seaFoodId") Integer seaFoodId) {
		Optional<String> resultOpt = classifierService.getId3Suggesting(seaFoodId);
		if (!resultOpt.isPresent()) {
			return new ResponseEntity<String>("not found seaFoodId", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(resultOpt.get().replaceFirst("Id3\\n\\n\\n", ""), HttpStatus.OK);
	}
}
