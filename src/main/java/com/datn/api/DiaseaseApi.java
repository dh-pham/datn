package com.datn.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.symptom.SymptomValue;
import com.datn.service.symptom.ISymptomValueService;

@RestController
@RequestMapping("/api")
public class DiaseaseApi {
	
	@Autowired
	private ISymptomValueService symptomValueService;
	
	@RequestMapping(value = "/getPopularSymptomValue/{id}")
	public List<SymptomValue> getPopularSymptomValueForAnDiasease(@PathVariable(value = "id") Integer diaseaseId) {
		return symptomValueService.getPopularSymptomValueForAnDiasease(diaseaseId);
	}

}
