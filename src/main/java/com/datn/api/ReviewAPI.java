package com.datn.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.review.LikeReview;
import com.datn.service.review.ILikeReviewService;

@RestController
@RequestMapping("/api")
public class ReviewAPI {
	
	@Autowired
	private ILikeReviewService likeReviewService;
	
	@RequestMapping(value = "/likeReviews/reviews/{reviewId}/users/{userId}", method = RequestMethod.GET)
	public LikeReview getLikeReview(@PathVariable(name = "reviewId") Integer reviewId,
			@PathVariable(name = "userId") Long userId) {
		Optional<LikeReview> likeReviewOptional = likeReviewService.getByReviewAndUser(reviewId, userId);
		if (!likeReviewOptional.isPresent()) {
			return null;
		} else {
			return likeReviewOptional.get();
		}
	}
	
	@RequestMapping(value = "/likeReviews/diaseases/{diaseaseId}/users/{userId}", method = RequestMethod.GET)
	public List<LikeReview> getLikeReviews(@PathVariable(name = "diaseaseId") Integer diaseaseId,
			@PathVariable(name = "userId") Long userId) {
		return likeReviewService.getByDiaseaseAndUser(diaseaseId, userId);
	}
}
