package com.datn.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.datn.service.auth.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
//@Order(SecurityProperties.BASIC_AUTH_ORDER)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();

//		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
//		http.authorizeRequests().antMatchers("/js/**", "/css/**", "/img/**").permitAll();

		http
			.authorizeRequests()
				.antMatchers("/signin", "/signup").anonymous()
				.antMatchers("/", "/diaseases", "/diaseases/diagnosis", "/diaseases/{id}", "/policy").permitAll()
				.antMatchers("/users/**").access("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_EXPERT')")
				.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
				.antMatchers("/secure_api/**").access("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_EXPERT')")
				.antMatchers("/api/**").permitAll()
				.anyRequest().authenticated()
				.and()
				.formLogin()
					.loginPage("/signin")
					.loginProcessingUrl("/perform_signin")
					.usernameParameter("email")
                	.passwordParameter("password")
					.defaultSuccessUrl("/", true).failureUrl("/signin?error=true")
					.and()
				.logout().logoutUrl("/perform_logout")
					.deleteCookies("JSESSIONID").logoutSuccessUrl("/signin")
				.and()
					.exceptionHandling()
					.accessDeniedPage("/403");
	}
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
            .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/img/**");
    }
}
