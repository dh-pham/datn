package com.datn.controller.auth;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthController {
	private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
	
	@GetMapping(value="/signin")
	public String showLogin(Principal principal) {
		if (principal != null) {
			return "redirect:/";
		}
		return "auth/signin";
	}
	
	@GetMapping(value="/userInfos")
	public String userInfo() {
		return "auth/user_info";
	}
	
	@GetMapping(value="/signup")
	public String showSignUp(Principal principal) {
		if (principal != null) {
			return "redirect:/";
		}
		return "auth/signup";
	}
}
