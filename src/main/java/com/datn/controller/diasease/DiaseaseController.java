package com.datn.controller.diasease;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@RequestMapping(value = "/diaseases")
@Controller
public class DiaseaseController {
	private static final Logger logger = LoggerFactory.getLogger(DiaseaseController.class);

	@RequestMapping(value="", method = RequestMethod.GET)
	public ModelAndView showDiaseaseSearch(@RequestParam(name = "q", required = false) String keyword,
			@RequestParam(name="page", required = false) Integer pageIndex) {
		ModelAndView modelAndView = new ModelAndView("diasease/diasease_search");
		return modelAndView;
	}
	
	@RequestMapping(value="/{id}", method = RequestMethod.GET)
	public ModelAndView showDiaseaseDetail(@PathVariable(value = "id") Integer id) {
		ModelAndView modelAndView = new ModelAndView("diasease/diasease_detail");
		return modelAndView;
	}
	
	@RequestMapping(value = "/diagnosis", method = RequestMethod.GET)
	public String showDiagnosisView() {
		return "diagnosis/diagnosis";
	}
}
