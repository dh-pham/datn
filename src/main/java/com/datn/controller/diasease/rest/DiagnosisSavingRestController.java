package com.datn.controller.diasease.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.SeaFood;
import com.datn.entity.auth.AppUser;
import com.datn.entity.diasease.DiagnosisSaving;
import com.datn.service.auth.IUserDetailsService;
import com.datn.service.diasease.IDiagnosisSavingService;
import com.datn.service.seafood.ISeaFoodService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/secure_api")
public class DiagnosisSavingRestController {

	@Autowired
	private IDiagnosisSavingService savingService;

	@Autowired
	private IUserDetailsService userService;

	@Autowired
	private ISeaFoodService seaFoodService;

	@RequestMapping(value = "/diagnosis/save", method = RequestMethod.POST)
	public ResponseEntity<String> save(@RequestBody JsonNode dataNode) {
		Integer seaFoodId = dataNode.path("seaFoodId").asInt();
		Optional<SeaFood> seaFoodOpt = seaFoodService.getById(seaFoodId);
		String symptoms = dataNode.path("symptoms").asText();
		String diaseases = dataNode.path("diaseases").asText();
		Optional<AppUser> currentUser = userService.getCurrentAppUser();
		if (currentUser.isPresent() && seaFoodOpt.isPresent()) {
			DiagnosisSaving saving = new DiagnosisSaving();
			saving.setAppUser(currentUser.get());
			saving.setSeaFood(seaFoodOpt.get());
			saving.setSymptoms(symptoms);
			saving.setDiaseases(diaseases);
			DiagnosisSaving result = savingService.save(saving);
			String resultStr;
			try {
				resultStr = new ObjectMapper().writeValueAsString(result);
				return new ResponseEntity<String>(resultStr, HttpStatus.OK);
			} catch (JsonProcessingException e) {
				return new ResponseEntity<String>("JsonProcessingException", HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
		return new ResponseEntity<String>("Bad input", HttpStatus.BAD_REQUEST);
	}

	@GetMapping(value = "/diagnosis/saved")
	public ResponseEntity<String> getSavingByUserId() {
		try {
			Optional<AppUser> currentUser = userService.getCurrentAppUser();
			if (currentUser.isPresent()) {
				List<DiagnosisSaving> saved = savingService.getByUserId(currentUser.get().getId());
				String response;

				response = new ObjectMapper().writeValueAsString(saved);

				return new ResponseEntity<String>(response, HttpStatus.OK);
			} else {
				return new ResponseEntity<String>("unauthorized!", HttpStatus.UNAUTHORIZED);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return new ResponseEntity<String>("server error!", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping(value = "/diagnosis/save/{id}")
	public ResponseEntity<String> delete(@PathVariable(name = "id") Integer saveId) {
		Optional<AppUser> currentUser = userService.getCurrentAppUser();
		if (!currentUser.isPresent())
			return new ResponseEntity<String>("Not authenticated", HttpStatus.UNAUTHORIZED);
		Optional<DiagnosisSaving> saveOpt = savingService.getById(saveId);
		if (!saveOpt.isPresent()) 
			return new ResponseEntity<String>("not found", HttpStatus.NOT_FOUND);
		if (saveOpt.get().getAppUser().getId() != currentUser.get().getId()) {
			return new ResponseEntity<>("permission denied", HttpStatus.FORBIDDEN);
		}
		savingService.delete(saveOpt.get());
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
}
