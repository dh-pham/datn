package com.datn.controller.review;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.auth.AppUser;
import com.datn.entity.review.LikeReview;
import com.datn.entity.review.Review;
import com.datn.service.auth.IUserDetailsService;
import com.datn.service.review.ILikeReviewService;
import com.datn.service.review.IReviewService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = "/api")
public class LikeReviewRestController {

	@Autowired
	private ILikeReviewService likeReviewService;

	@Autowired
	private IUserDetailsService userService;

	@Autowired
	private IReviewService reviewService;

	@RequestMapping(value = "/likeReviews", method = RequestMethod.POST)
	public ResponseEntity<String> create(@RequestBody JsonNode likeReviewNode, Authentication authentication) {
		try {
			if (authentication == null) {
				return new ResponseEntity<String>("Not be authenticated!", HttpStatus.UNAUTHORIZED);
			}
			Integer reviewId = likeReviewNode.path("reviewId").asInt();
			Long userId = likeReviewNode.path("userId").asLong();

			UserDetails userDetails = (UserDetails) authentication.getPrincipal();
			AppUser currentUser = userService.findByEmail(userDetails.getUsername());

			if (!currentUser.getId().equals(userId)) {
				return new ResponseEntity<String>("Permission denied!", HttpStatus.FORBIDDEN);
			}

			Optional<Review> reviewOptional = reviewService.getById(reviewId);
			if (!reviewOptional.isPresent()) {
				return new ResponseEntity<String>("Resource not found!", HttpStatus.NOT_FOUND);
			}
			LikeReview likeReview = new LikeReview(reviewOptional.get(), currentUser);
			ObjectMapper mapper = new ObjectMapper();

			String response = mapper.writeValueAsString(likeReviewService.create(likeReview));
			return new ResponseEntity<String>(response, HttpStatus.OK);
		} catch (JsonProcessingException e) {
			return new ResponseEntity<String>("Internal error!", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

	@RequestMapping(value = "/likeReviews/{likeReviewId}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable(name = "likeReviewId") Integer likeReviewId,
			Authentication authentication) {
		Optional<LikeReview> likeReviewOptional = likeReviewService.getById(likeReviewId);
		if (!likeReviewOptional.isPresent()) {
			return new ResponseEntity<String>("Resource not found", HttpStatus.NOT_FOUND);
		}
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		AppUser currentUser = userService.findByEmail(userDetails.getUsername());
		LikeReview likeReview = likeReviewOptional.get();
		if (likeReview.getAppuser().getId() != currentUser.getId()) {
			return new ResponseEntity<String>("Permission denied", HttpStatus.FORBIDDEN);
		}
		likeReviewService.delete(likeReview);
		return new ResponseEntity<String>("Delete request successfully", HttpStatus.OK);
	}
}
