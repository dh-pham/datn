package com.datn.controller.review;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.auth.AppUser;
import com.datn.entity.diasease.Diasease;
import com.datn.entity.review.Review;
import com.datn.service.auth.IUserDetailsService;
import com.datn.service.diasease.IDiaseaseService;
import com.datn.service.review.IReviewService;
import com.fasterxml.jackson.databind.JsonNode;;

@RestController
@RequestMapping(value = "/api")
public class ReviewRestController {
	
	private static final Logger logger = LoggerFactory.getLogger(ReviewRestController.class);
	
	@Autowired
	private IReviewService reviewService;
	
	@Autowired
	private IUserDetailsService userService;
	
	@Autowired
	private IDiaseaseService diaseaseService;
	
	@RequestMapping(value = "/reviews", method = RequestMethod.POST)
	public ResponseEntity<String> create(@RequestBody JsonNode reviewNode, Authentication authentication) {
		if (authentication == null) {
			return new ResponseEntity<String>("Not be authenticated!", HttpStatus.UNAUTHORIZED);
		}
		Integer sentDiaseaseId = reviewNode.path("diaseaseId").asInt();
		Integer score = reviewNode.path("score").asInt();
		String comment = reviewNode.path("comment").asText();
		
		UserDetails userDetails =  (UserDetails) authentication.getPrincipal();
		AppUser currentUser = userService.findByEmail(userDetails.getUsername());
		Optional<Diasease> diaseaseOptional = diaseaseService.getById(sentDiaseaseId);
		if (!diaseaseOptional.isPresent()) {
			return new ResponseEntity<String>("Not found diasease resource", HttpStatus.NOT_FOUND);
		} else {
			Review review = new Review(currentUser, diaseaseOptional.get(), score, comment);
			return new ResponseEntity<String>(reviewService.create(review).toString(), HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/reviews/{reviewId}", method = RequestMethod.PUT)
	public ResponseEntity<String> update(@PathVariable(name = "reviewId") Integer reviewId,
		@RequestBody JsonNode sentNode, Authentication authentication) {
		if (authentication == null) {
			return new ResponseEntity<String>("Not be authenticated!", HttpStatus.UNAUTHORIZED);
		}
		Integer score = sentNode.path("score").asInt();
		String comment = sentNode.path("comment").asText();
		UserDetails userDetails =  (UserDetails) authentication.getPrincipal();
		AppUser currentUser = userService.findByEmail(userDetails.getUsername());
		//1. find review by Id
		Optional<Review> reviewOptional = reviewService.getById(reviewId);
		if (!reviewOptional.isPresent()) {
			return new ResponseEntity<String>("Resource not found!", HttpStatus.NOT_FOUND);
		} 
		//2. authorized
		Review review = reviewOptional.get();
		if (!review.getAppUser().getId().equals(currentUser.getId())) {
			return new ResponseEntity<String>("Permission denied!", HttpStatus.FORBIDDEN);
		}
		//3. save
		review.setScore(score);
		review.setComment(comment);
		review.setTime(new Timestamp(new Date().getTime()));
		return new ResponseEntity<String>(reviewService.update(review).toString(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "reviews/{reviewId}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable(name = "reviewId") Integer reviewId,
			Authentication authentication) {
		Optional<Review> reviewOptional = reviewService.getById(reviewId);
		if (!reviewOptional.isPresent()) {
			return new ResponseEntity<String>("Resource not found", HttpStatus.NOT_FOUND);
		}
		UserDetails userDetails =  (UserDetails) authentication.getPrincipal();
		AppUser currentUser = userService.findByEmail(userDetails.getUsername());
		Review review = reviewOptional.get();
		if (!review.getAppUser().getId().equals(currentUser.getId())) {
			return new ResponseEntity<String>("Permission denied", HttpStatus.FORBIDDEN);
		}
		reviewService.delete(review);
		return new ResponseEntity<String>("Delete request successfully", HttpStatus.OK);
	}
}
