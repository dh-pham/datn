package com.datn.controller.symptom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.symptom.Symptom;
import com.datn.service.symptom.ISymptomService;

@RestController
@RequestMapping("/api")
public class SymptomRestController {
	
	@Autowired
	private ISymptomService symptomService;
	
	@GetMapping(value = "/symptoms")
	public List<Symptom> getAll() {
		List<Symptom> symptoms = symptomService.getAll();
		return symptoms;
	}
}
