package com.datn.controller.symptom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.symptom.SymptomType;
import com.datn.service.symptom.ISymptomTypeService;

@RestController
@RequestMapping("/api")
public class SymptomTypeRestController {

	@Autowired
	private ISymptomTypeService symptomTypeService;
	
	@GetMapping("/symptomTypes")
	public List<SymptomType> getAll() {
		return symptomTypeService.getAll();
		
	}
}

