package com.datn.controller.symptom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.datn.entity.symptom.SymptomValue;
import com.datn.service.symptom.ISymptomValueService;

@RestController
@RequestMapping("/api")
public class SymptomValueRestController {
	@Autowired
	private ISymptomValueService symptomValueService;
	
	@GetMapping("/symptomValues")
	public List<SymptomValue> getAll() {
		return symptomValueService.getAll();
	}
	
}
