package com.datn.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.datn.entity.diasease.DiagnosisSaving;
import com.datn.entity.diasease.Diasease;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="seaFood")
public class SeaFood {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="seaFoodName")
	private String seaFoodName;
	
	@JsonBackReference
	@OneToMany(mappedBy="seaFood")
	private List<Diasease> diaseases;
	
	@JsonBackReference
	@OneToMany(mappedBy = "seaFood")
	private List<DiagnosisSaving> diagnosisSavings;

	@JsonBackReference
	public List<DiagnosisSaving> getDiagnosisSavings() {
		return diagnosisSavings;
	}

	@JsonBackReference
	public void setDiagnosisSavings(List<DiagnosisSaving> diagnosisSavings) {
		this.diagnosisSavings = diagnosisSavings;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSeaFoodName() {
		return seaFoodName;
	}

	public void setSeaFoodName(String seaFoodName) {
		this.seaFoodName = seaFoodName;
	}

	@JsonBackReference
	public List<Diasease> getDiaseases() {
		return diaseases;
	}

	@JsonBackReference
	public void setDiaseases(List<Diasease> diaseases) {
		this.diaseases = diaseases;
	}
	
}
