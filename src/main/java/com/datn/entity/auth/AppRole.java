package com.datn.entity.auth;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="role")
public class AppRole {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@JsonBackReference
	@OneToMany(mappedBy="appRole", fetch = FetchType.LAZY)
	private List<AppUser> appUsers;
	
	public AppRole() {
		super();
	}

	public AppRole(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	// getter setter
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonBackReference
	public List<AppUser> getUsers() {
		return appUsers;
	}
	
	@JsonBackReference
	public void setUsers(List<AppUser> users) {
		this.appUsers = users;
	}
	
}
