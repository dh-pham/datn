package com.datn.entity.auth;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.datn.entity.diasease.DiagnosisSaving;
import com.datn.entity.review.LikeReview;
import com.datn.entity.review.Review;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="user")
public class AppUser {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="email")
	private String email;
	
	@Column(name="encryptedPassword")
	private String encryptedPassword;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="roleId")
	private AppRole appRole = new AppRole(new Long(3), "ROLE_USER");
	
	@JsonBackReference
	@OneToMany(mappedBy = "appUser", fetch = FetchType.LAZY)
	private List<Review> reviews;
	
	@JsonBackReference
	@OneToMany(mappedBy = "appUser", fetch = FetchType.LAZY)
	private List<DiagnosisSaving> diagnosisSavings;
	
	@Column(name="enabled")
	private Integer enabled = 0;
	
	@Column(name="startedAt")
	private Timestamp startedAt = new Timestamp(new Date().getTime());
	
	@Column(name = "image")
	private String image;
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@JsonBackReference
	@OneToMany(mappedBy = "appUser", fetch = FetchType.LAZY)
	private List<LikeReview> likeReviews;
	

	public AppUser() {
		super();
	}

	public AppUser(String name, String email, String encrytedPassword) {
		super();
		this.name = name;
		this.email = email;
		this.encryptedPassword = encrytedPassword;
	}

	// getter setter
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	@JsonBackReference
	public List<Review> getReviews() {
		return reviews;
	}

	@JsonBackReference
	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	@JsonManagedReference
	public AppRole getAppRole() {
		return appRole;
	}

	@JsonManagedReference
	public void setAppRole(AppRole appRole) {
		this.appRole = appRole;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Timestamp getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Timestamp startedAt) {
		this.startedAt = startedAt;
	}
	
	@JsonBackReference
	public List<LikeReview> getLikeReviews() {
		return likeReviews;
	}

	@JsonBackReference
	public void setLikeReviews(List<LikeReview> likeReviews) {
		this.likeReviews = likeReviews;
	}
	
}
