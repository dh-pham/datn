package com.datn.entity.diasease;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="diaseaseAndSymptom")
public class DiaseaseAndSymptom {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="diaseaseId")
	private Diasease diasease;
	
	@JsonBackReference
	@OneToMany(mappedBy = "diaseaseAndSymptom")
	private List<DiaseaseAndSymptomLine> diaseaseAndSymptomLines;
	
	@JsonBackReference
	public List<DiaseaseAndSymptomLine> getDiaseaseAndSymptomLines() {
		return diaseaseAndSymptomLines;
	}

	@JsonBackReference
	public void setDiaseaseAndSymptomLines(List<DiaseaseAndSymptomLine> diaseaseAndSymptomLines) {
		this.diaseaseAndSymptomLines = diaseaseAndSymptomLines;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonManagedReference
	public Diasease getDiasease() {
		return diasease;
	}

	@JsonManagedReference
	public void setDiasease(Diasease diasease) {
		this.diasease = diasease;
	}

}
