package com.datn.entity.review;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.datn.entity.auth.AppUser;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "likeReview")
public class LikeReview {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "reviewId")
	private Review review;
	
	@JsonManagedReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "reviewUserId")
	private AppUser appUser;

	
	
	public LikeReview() {
		super();
	}

	public LikeReview(Review review, AppUser appUser) {
		super();
		this.review = review;
		this.appUser = appUser;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonManagedReference
	public Review getReview() {
		return review;
	}
	
	@JsonManagedReference
	public void setReview(Review review) {
		this.review = review;
	}

	@JsonManagedReference
	public AppUser getAppuser() {
		return appUser;
	}

	@JsonManagedReference
	public void setAppuser(AppUser appuser) {
		this.appUser = appuser;
	}
	
	
}
