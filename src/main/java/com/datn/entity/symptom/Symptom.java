package com.datn.entity.symptom;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="symptom")
public class Symptom {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="symptomName")
	private String symptomName;
	
	@Column(name="symptomQuestion")
	private String symptomQuestion;
	
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name="symptomTypeId")
	private SymptomType symptomType;
	
	@JsonBackReference
	@OneToMany(mappedBy="symptom", fetch = FetchType.LAZY)
	private List<SymptomValue> symptomValues;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSymptomName() {
		return symptomName;
	}

	public void setSymptomName(String symptomName) {
		this.symptomName = symptomName;
	}

	public String getSymptomQuestion() {
		return symptomQuestion;
	}

	public void setSymptomQuestion(String symptomQuestion) {
		this.symptomQuestion = symptomQuestion;
	}

	@JsonManagedReference
	public SymptomType getSymptomType() {
		return symptomType;
	}

	@JsonManagedReference
	public void setSymptomType(SymptomType symptomType) {
		this.symptomType = symptomType;
	}

	@JsonBackReference
	public List<SymptomValue> getSymptomValues() {
		return symptomValues;
	}

	@JsonBackReference
	public void setSymptomValues(List<SymptomValue> symptomValues) {
		this.symptomValues = symptomValues;
	}
	
	
	
	
	
	
}
