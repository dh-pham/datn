package com.datn.entity.symptom;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="symptomType")
public class SymptomType {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="symptomTypeName")
	private String symptomTypeName;
	
	@JsonBackReference
	@OneToMany(mappedBy="symptomType", fetch = FetchType.LAZY)
	private List<Symptom> symptoms;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSymptomTypeName() {
		return symptomTypeName;
	}

	public void setSymptomTypeName(String symptomTypeName) {
		this.symptomTypeName = symptomTypeName;
	}

	@JsonBackReference
	public List<Symptom> getSymptoms() {
		return symptoms;
	}

	@JsonBackReference
	public void setSymptoms(List<Symptom> symptoms) {
		this.symptoms = symptoms;
	}

	
}
