package com.datn.entity.symptom;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.datn.entity.diasease.DiaseaseAndSymptomLine;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="symptomValue")
public class SymptomValue {
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name="symptomId")
	private Symptom symptom;
	
	@Column(name="symptomValueDesc")
	private String symptomValueDesc;
	
	@Column(name="image")
	private String image;
	
	@Column(name="indexNumber")
	private Integer indexNumber;
	
	@JsonBackReference
	@OneToMany(mappedBy="symptomValue")
	private List<DiaseaseAndSymptomLine> diaseaseAndSymptomLines;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonManagedReference
	public Symptom getSymptom() {
		return symptom;
	}

	@JsonManagedReference
	public void setSymptom(Symptom symptom) {
		this.symptom = symptom;
	}

	public String getSymptomValueDesc() {
		return symptomValueDesc;
	}

	public void setSymptomValueDesc(String symptomValueDesc) {
		this.symptomValueDesc = symptomValueDesc;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(Integer indexNumber) {
		this.indexNumber = indexNumber;
	}

	@JsonBackReference
	public List<DiaseaseAndSymptomLine> getDiaseaseAndSymptomLines() {
		return diaseaseAndSymptomLines;
	}

	@JsonBackReference
	public void setDiaseaseAndSymptomLines(List<DiaseaseAndSymptomLine> diaseaseAndSymptomLines) {
		this.diaseaseAndSymptomLines = diaseaseAndSymptomLines;
	}


	
	
}
