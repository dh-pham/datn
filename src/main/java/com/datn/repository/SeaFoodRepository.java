package com.datn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.datn.entity.SeaFood;
import com.datn.entity.symptom.SymptomValue;

@Repository
public interface SeaFoodRepository extends JpaRepository<SeaFood, Integer>{
	
//	@Query(value = "select * from symptomValue where id in (\n" + 
//			"	select symptomValueId from diaseaseAndSymptomLine where diaseaseAndSymptomId in (\n" + 
//			"		select id from diaseaseAndSymptom where diaseaseId in (\n" + 
//			"			select id from diasease where seaFoodId = ?1\n" + 
//			"        )\n" + 
//			"    )\n" + 
//			") and symptomId = ?2",
//			nativeQuery = true)
//	public List<SymptomValue> getSymptomValues(Integer seaFoodId, Integer symptomId);
	
	@Query("select sv from SymptomValue sv where sv in (\n" + 
			"	select dsl.symptomValue from DiaseaseAndSymptomLine dsl where dsl.diaseaseAndSymptom in (\n" + 
			"		select das from DiaseaseAndSymptom das where das.diasease in (\n" + 
			"			select d from Diasease d where d.seaFood.id = ?1\n" + 
			"        )\n" + 
			"    )\n" + 
			") and sv.symptom.id = ?2")
	public List<SymptomValue> getSymptomValues(Integer seaFoodId, Integer symptomId);
}
