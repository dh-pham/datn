package com.datn.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datn.entity.auth.AppRole;

@Repository
public interface AppRoleRepository extends JpaRepository<AppRole, Long>{

}
