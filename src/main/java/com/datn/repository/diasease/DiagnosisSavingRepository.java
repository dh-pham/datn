package com.datn.repository.diasease;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.datn.entity.diasease.DiagnosisSaving;

@Repository
public interface DiagnosisSavingRepository extends JpaRepository<DiagnosisSaving, Integer>{
	@Query("SELECT d FROM DiagnosisSaving d WHERE d.appUser.id = ?1")
	public List<DiagnosisSaving> getSavingByUserId(Long userId);
}
