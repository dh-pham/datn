package com.datn.repository.diasease;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.datn.entity.diasease.DiaseaseAndSymptom;

@Repository
public interface DiaseaseAndSymptomRepository extends JpaRepository<DiaseaseAndSymptom, Long>{
	
	@Query("SELECT d FROM DiaseaseAndSymptom d WHERE d.diasease.seaFood.id = ?1")
	public List<DiaseaseAndSymptom> getBySeaFoodId(int seaFoodId);
	
}
