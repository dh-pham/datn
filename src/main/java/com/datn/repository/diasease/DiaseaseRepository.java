package com.datn.repository.diasease;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.datn.entity.diasease.Diasease;
import com.datn.entity.review.Review;

@Repository
public interface DiaseaseRepository extends JpaRepository<Diasease, Integer>{
	public Page<Diasease> findAll(Pageable pageable);
	
	@Query("SELECT d FROM Diasease d WHERE d.diaseaseName LIKE ?1")
	public List<Diasease> searchByDiaseaseName(String param);
	
	@Query("SELECT d from Diasease d WHERE d.seaFood.seaFoodName LIKE ?1")
	public List<Diasease> searchBySeaFoodName(String param);
	
	@Query("SELECT d from Diasease d WHERE d.seaFood.seaFoodName LIKE ?1 OR d.diaseaseName LIKE ?1")
	public Page<Diasease> searchByKeyword(String keyword, Pageable pageable);
	
	@Query("SELECT d.reviews from Diasease d WHERE d.id = ?1")
	public Page<Review> getReviews(Integer diaseaseId, Pageable pageable);
}
