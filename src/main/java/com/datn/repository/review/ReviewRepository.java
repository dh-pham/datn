package com.datn.repository.review;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datn.entity.review.Review;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Integer>{

	
}
