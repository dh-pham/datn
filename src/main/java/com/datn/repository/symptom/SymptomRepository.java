package com.datn.repository.symptom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datn.entity.symptom.Symptom;

@Repository
public interface SymptomRepository extends JpaRepository<Symptom, Integer>{

}
