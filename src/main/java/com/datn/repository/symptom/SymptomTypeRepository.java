package com.datn.repository.symptom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.datn.entity.symptom.SymptomType;

@Repository
public interface SymptomTypeRepository extends JpaRepository<SymptomType, Integer>{

}