package com.datn.repository.symptom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.datn.entity.symptom.SymptomValue;

@Repository
public interface SymptomValueRepository extends JpaRepository<SymptomValue, Long>{
	
	@Query(
		value = "select * from symptomValue " + 
				"where id in " + 
				"(select t1.valueId " + 
				"	from ( " + 
				"		select d2.symptomValueId as valueId, count(d2.symptomValueId) as count " + 
				"		from diaseaseAndSymptom d1 inner join diaseaseAndSymptomLine d2 " + 
				"		on d1.id = d2.diaseaseAndSymptomId " + 
				"		where d1.diaseaseId = ?1 " + 
				"		group by(d2.symptomValueId) " + 
				"	) as t1 " + 
				"	where t1.count = (select count(*) from diaseaseAndSymptom where diaseaseId = ?1) " + 
				")",
		nativeQuery = true
	)
	List<SymptomValue> getPopularSymptomValueForAnDiasease(Integer diaseaseId);
	
	@Query("SELECT s from SymptomValue s WHERE s.symptom.id = ?1")
	List<SymptomValue> getBySymptomId(Integer symptomId);
}