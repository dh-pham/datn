package com.datn.service.auth;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.datn.entity.auth.AppUser;

public interface IUserDetailsService extends UserDetailsService{
	public AppUser findByEmail(String email);
	public AppUser save(AppUser user);
	public Optional<AppUser> findById(Long id);
	public Optional<AppUser> getCurrentAppUser();
//	public void signin(AppUser appUser);
}
