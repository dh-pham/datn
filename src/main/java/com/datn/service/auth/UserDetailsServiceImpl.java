package com.datn.service.auth;

//import java.util.ArrayList;
import java.util.Collections;
//import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.datn.entity.auth.AppUser;
import com.datn.repository.auth.AppUserRepository;

@Service
public class UserDetailsServiceImpl implements IUserDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

	@Autowired
	private AppUserRepository appUserRepo;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		AppUser appUser = findByEmail(email);
		if (appUser == null) {
			logger.debug("User not found! " + email);
			throw new UsernameNotFoundException("User " + email + " was not found in the database");
		}
		logger.info("Found User: " + appUser.getEmail());
		return createUser(appUser);
	}

	@Override
	public AppUser findByEmail(String email) {
		Optional<AppUser> appUser = appUserRepo.findByEmail(email);
		if (appUser.isPresent()) {
			return appUser.get();
		}
		return null;
	}

	@Override
	public AppUser save(AppUser user) {
		return appUserRepo.save(user);
	}

//	@Override
//	public void signin(AppUser appUser) {
//		SecurityContextHolder.getContext().setAuthentication(authentication(appUser));
//	}
//
//	private Authentication authentication(AppUser appUser) {
//		return new UsernamePasswordAuthenticationToken(createUser(appUser), null,
//				Collections.singleton(createAuthority(appUser)));
//	}

	private User createUser(AppUser appUser) {
		return new User(appUser.getEmail(), appUser.getEncryptedPassword(),
				Collections.singleton(createAuthority(appUser)));
	}

	private GrantedAuthority createAuthority(AppUser appUser) {
		return new SimpleGrantedAuthority(appUser.getAppRole().getName());
	}

	@Override
	public Optional<AppUser> findById(Long id) {
		return appUserRepo.findById(id);
	}

	@Override
	public Optional<AppUser> getCurrentAppUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
			String currentUserName = authentication.getName();
			return Optional.ofNullable(findByEmail(currentUserName));
		} else {
			return Optional.empty();
		}
	}

}