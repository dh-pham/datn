package com.datn.service.classifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.SeaFood;
import com.datn.entity.diasease.Diasease;
import com.datn.entity.diasease.DiaseaseAndSymptom;
import com.datn.entity.diasease.DiaseaseAndSymptomLine;
import com.datn.entity.symptom.Symptom;
import com.datn.entity.symptom.SymptomValue;
import com.datn.service.diasease.IDiaseaseAndSymptomService;
import com.datn.service.seafood.ISeaFoodService;
import com.datn.service.symptom.ISymptomService;
import com.datn.service.symptom.ISymptomValueService;
import com.datn.utils.ConstraintUtil;
import com.datn.utils.OtherUtil;

import weka.classifiers.Evaluation;
import weka.classifiers.trees.Id3;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ConverterUtils.DataSink;

@Transactional
@Service
public class ClassifierService implements IClassifierService {

	private static final Logger logger = LoggerFactory.getLogger(ClassifierService.class);

	@Autowired
	private ISeaFoodService seaFoodService;

	@Autowired
	private ISymptomService symptomService;

	@Autowired
	private ISymptomValueService symptomValueService;

	@Autowired
	private IDiaseaseAndSymptomService diaseaseAndSymptomService;

	private Map<Integer, RandomForest> classifierMap = new HashMap<Integer, RandomForest>();

	private Map<Integer, Id3> id3SuggestingTree = new HashMap<Integer, Id3>();

	private Map<Integer, Instances> instancesTrainingMap = new HashMap<Integer, Instances>();

	@Override
	public void loadDataOrCreateIfNotExisted() {

		File dataDir = new File(ConstraintUtil.DATA_DIR);
		if (dataDir.exists()) {
			loadDataFromArff();
		} else {
			forcusRenewDataAndArff();
			arffGenerating();
		}
	}

	// dataType = 0 (original), 1 (training), 2 (testing)
	@Override
	public void forcusRenewDataAndArff() {
		File dataDir = new File(ConstraintUtil.DATA_DIR);
		if (dataDir.exists()) {
			try {
				OtherUtil.deleteDirectoryRecursion(dataDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		List<SeaFood> seaFoods = seaFoodService.getAll();
		for (SeaFood seaFood : seaFoods) {
			int seaFoodId = seaFood.getId();
			if (seaFoodId > 0) {
				logger.info("Init data for seaFoodId=" + seaFoodId);
				Instances seaFoodInstances = generateInstances(seaFoodId, 0.5F, 0.3F, 50);
				if (seaFoodInstances != null) {
					logger.info("create instances, classifier done!");
					this.classifierMap.put(seaFoodId, createClassifier(seaFoodInstances));
					this.id3SuggestingTree.put(seaFoodId, createId3Tree(seaFoodInstances));
					this.instancesTrainingMap.put(seaFoodId, seaFoodInstances);
					logger.info("id3Suggesting tree creatated!");
				}
			}
		}
		logger.info("Forcus renew data and arff file done!");
	}

//	@Override
//	public String predict(int seaFoodId, List<String> symptomValueIds) {
//		RandomForest classifier = getClassifier(seaFoodId);
//		if (classifier == null)
//			return null;
//		Instance instance = prepareInstance(seaFoodId, symptomValueIds);
//		try {
//			double[] dbs = classifier.distributionForInstance(instance);
//			for (int i = 0; i < dbs.length; i++) {
//				System.out.println(i + ":" + dbs[i]);
//			}
//			int result = (int) classifier.classifyInstance(instance);
//			String readableResult = getTrainingInstances(seaFoodId, true).classAttribute().value(result);
//			return readableResult;
//		} catch (Exception e1) {
//			e1.printStackTrace();
//			return null;
//		}
//	}

	@Override
	public Map<String, Double> getTopResultWithPercentage(int number, int seaFoodId, List<String> symptomValueIds) {
		Instances instances = new Instances(getTrainingInstances(seaFoodId, true));
		instances = modifyInstances(instances, symptomValueIds);
		RandomForest classifier = createClassifier(instances);
		
		logger.info("nums of tree: " + classifier.getNumTrees());
		logger.info("nums of feature: " + classifier.getNumFeatures());
		if (classifier == null)
			return null;
		Instance instance = prepareInstance(seaFoodId, symptomValueIds, instances);
		try {
			double[] dbs = classifier.distributionForInstance(instance);
			for (int i = 0; i < dbs.length; i++) {
				System.out.println(i + ":" + dbs[i]);
			}
			List<Integer> indexTop = OtherUtil.getIndexsOfTopMaxEle(number, dbs);
			logger.info(indexTop.toString());
			Map<String, Double> resultMap = new HashMap<>();
			for (Integer index : indexTop) {
				String diaseaseId = instance.classAttribute().value(index);
				Double percentage = dbs[index];
				resultMap.put(diaseaseId, percentage);
			}
			return resultMap;

		} catch (Exception e1) {
			e1.printStackTrace();
			return null;
		}
	}
	
	private Instances modifyInstances(Instances instances, List<String> values) {
		List<String> deleteAtts = new ArrayList<>();
		for (String value: values) {
			String symptomId = getSymptomIdFromValue(Long.valueOf(value));
			if (Long.valueOf(value) < 0 || Arrays.asList(ConstraintUtil.weakFeats).contains(symptomId)) {
				deleteAtts.add(symptomId);
			}
		}
		
		int i = 0;
		while (i != instances.classIndex()) {
			if (deleteAtts.contains(instances.attribute(i).name())) {
				logger.info("deleting att: " + instances.attribute(i).name());
				instances.deleteAttributeAt(i);
				logger.info("deleted");
			} else {
				i = i + 1;
			}
		}
		logger.info("num of att after delete: " + instances.numAttributes());
		logger.info("class att: " + instances.classIndex());
		return instances;
		
	}

	@Override
	public Optional<String> getId3Suggesting(int seaFoodId) {
		return Optional.ofNullable(getId3Tree(seaFoodId).toString());
	}

	public boolean arffGenerating() {
		if (instancesTrainingMap == null) {
			logger.error("instancesTrainingMap == null");
			return false;
		}

		for (Integer seaFoodId : instancesTrainingMap.keySet()) {
			Instances instances = instancesTrainingMap.get(seaFoodId);
			try {
				DataSink.write(convertToFileName(seaFoodId, 1), instances);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		logger.info("arff generated!");
		return true;

	}

	public void loadDataFromArff() {
		logger.info("Loading data from arff file...");
		try {
			File trainDir = new File(ConstraintUtil.TRAINING_DIR);
			if (trainDir.isDirectory()) {
				for (File file : trainDir.listFiles()) {
					BufferedReader reader = new BufferedReader(new FileReader(file));
					ArffReader arffReader = new ArffReader(reader);
					Instances trainingInstances = arffReader.getData();
					trainingInstances.setClassIndex(trainingInstances.numAttributes() - 1);
					setWeightForInstance(trainingInstances);
					Integer seaFoodId = Integer.valueOf(trainingInstances.relationName());
					logger.info("Loading data for seaFoodId=" + seaFoodId);
					this.instancesTrainingMap.put(seaFoodId, trainingInstances);
					this.classifierMap.put(seaFoodId, createClassifier(trainingInstances));
					this.id3SuggestingTree.put(seaFoodId, createId3Tree(trainingInstances));
				}
				logger.info("All data have loaded!");
			} else {
				logger.error("Expect directory but not in loadDataFromArff!");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setWeightForInstance(Instances instances) {
		for (int i = 0; i < instances.numAttributes(); i++) {
			String attName = instances.attribute(i).name();
			if (Arrays.asList(ConstraintUtil.weakFeats).contains(attName)) {
				instances.attribute(i).setWeight(0.2);
			} else if (Arrays.asList(ConstraintUtil.intermediateFeats).contains(attName)) {
				instances.attribute(i).setWeight(0.4);
			} else if (Arrays.asList(ConstraintUtil.strongFeats).contains(attName)) {
				instances.attribute(i).setWeight(1);
			}
		}
	}
	public void evaluating() {
		try {
			for (Integer seaFoodId : instancesTrainingMap.keySet()) {
				Instances testInstances = generateInstances(seaFoodId, 0.4F, 0.2F, 2);
				logger.info("weight of 0: " + testInstances.attribute(0).weight());
				logger.info("weight of 1: " + testInstances.attribute(1).weight());
				Evaluation testEva = new Evaluation(testInstances);
				testEva.evaluateModel(getClassifier(seaFoodId), testInstances);
				logger.info("Evaluating for: " + seaFoodId);
//				logger.info("train: " + trainEva.pctCorrect());
				logger.info("test: " + testEva.pctCorrect());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void evaluatingAdvance() {
		try {
			for (Integer seaFoodId : instancesTrainingMap.keySet()) {
				Instances testInstances = generateInstances(seaFoodId, 0.4F, 0.2F, 2);
				Evaluation testEva = new Evaluation(testInstances);
				Instances suggestTestInstances = new Instances(testInstances, 1000);
				String suggestion = getId3Suggesting(seaFoodId).get();
				String replacedStr = suggestion.replaceFirst("Id3\\n\\n\\n", "");
				Integer symptomSuggestionId = Integer.valueOf(replacedStr.split("\\s+")[0]);
				Integer numInstances = testInstances.numInstances();

				for (int i = 0; i < numInstances; i++) {
					String attValue = testInstances.instance(i).toString(symptomSuggestionId - 1);
					if (Integer.valueOf(attValue) > 0) {
						suggestTestInstances.add(testInstances.instance(i));
					}
				}
				testEva.evaluateModel(getClassifier(seaFoodId), suggestTestInstances);
				logger.info("Evaluating for: " + seaFoodId);
				logger.info("numbers of instance: " + suggestTestInstances.numInstances());
				logger.info("test: " + testEva.pctCorrect());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Id3 createId3Tree(Instances instances) {
		Id3 id3Tree = new Id3();
		try {
			id3Tree.buildClassifier(instances);
		} catch (Exception e) {
			return id3Tree;
		}
		return id3Tree;
	}

	// return null if exist an attribute dont have value
	private Instances generateInstances(int seaFoodId, float randomRatio, float defaultRatio, int recordsEachDiasease) {
		// add symptom attributes
		List<Symptom> symptoms = symptomService.getAll();
		FastVector allAttsVec = new FastVector();
		for (Symptom symptom : symptoms) {
			Attribute symptomAtt = createSymptomAtt(symptom);
			if (symptomAtt.numValues() == 0)
				return null;
			allAttsVec.addElement(symptomAtt);
		}
		// add label attribute
		Optional<SeaFood> optionalSeaFood = seaFoodService.getById(seaFoodId);
		if (!optionalSeaFood.isPresent()) {
			return null;
		} else {
			SeaFood seaFood = optionalSeaFood.get();
			Attribute labelAtt = createLabelAtt(seaFood);
			if (labelAtt.numValues() == 0)
				return null;
			allAttsVec.addElement(labelAtt);
			Instances insData = new Instances(String.valueOf(seaFoodId), allAttsVec, 100);
			insData.setClassIndex(insData.numAttributes() - 1);

			// add data
			List<DiaseaseAndSymptom> diaseaseAndSymptoms = diaseaseAndSymptomService.getBySeaFoodId(seaFoodId);
			for (DiaseaseAndSymptom symptomEle : diaseaseAndSymptoms) {
				Instance instance = new Instance(insData.numAttributes());
				instance.setDataset(insData);
				instance.setValue(insData.classIndex(), symptomEle.getDiasease().getId().toString());
				List<DiaseaseAndSymptomLine> symptomLines = symptomEle.getDiaseaseAndSymptomLines().stream()
						.sorted((a, b) -> a.getSymptomValue().getSymptom().getId()
								.compareTo(b.getSymptomValue().getSymptom().getId()))
						.collect(Collectors.toList());

				List<Long> values = symptomLines.stream().map(ele -> ele.getSymptomValue().getId())
						.collect(Collectors.toList());

				for (int i = 0; i < recordsEachDiasease; i++) {
					for (int j = 0; j < values.size(); j++) {
						String value = String.valueOf(generateValue(values.get(j), randomRatio, defaultRatio));
						instance.setValue(j, value);
					}
					insData.add(instance);
				}
			}
			return insData;
		}
	}

	private Long generateValue(Long num, float randomRatio, float defaultRatio) {
		int randomNum = new Random().nextInt(101);
		int typeOfFeat = getTypeOfFeat(num);
		Integer symptomId = Integer.valueOf(getSymptomIdFromValue(num));
		if (typeOfFeat == -1) { // weak
			if (randomNum < randomRatio * 100) {
				List<Long> values = symptomValueService
						.getBySymptomId(symptomId).stream().map(e -> e.getId())
						.collect(Collectors.toList());
				Integer index = new Random().nextInt(values.size());
				return values.get(index);
			} else {
				return num;
			}
		}
		
		if (typeOfFeat == 0) { // intermediate
			if (randomNum < defaultRatio * 100) {
//				return (num < 0) ? num : (-num / 10000);
				List<Long> values = symptomValueService
						.getBySymptomId(symptomId).stream().map(e -> e.getId())
						.collect(Collectors.toList());
				Integer index = new Random().nextInt(values.size());
				return values.get(index);
			} else {
				return num;
			}
		}
		
		if (typeOfFeat == 1) { // strong
			return num;
		}
		
		return null;
	}

	private String getSymptomIdFromValue(Long value) {
		if (value < 0)
			return String.valueOf(-value);
		return String.valueOf(value / 10000);
	}

	private Integer getTypeOfFeat(Long value) {
		String symptomId = getSymptomIdFromValue(value);
		if (Arrays.asList(ConstraintUtil.weakFeats).contains(symptomId))
			return -1;
		if (Arrays.asList(ConstraintUtil.intermediateFeats).contains(symptomId))
			return 0;
		if (Arrays.asList(ConstraintUtil.strongFeats).contains(symptomId))
			return 1;
		return null;
	}

	private Instance prepareInstance(int seaFoodId, List<String> symptomValueIds, Instances instances) {
//		Instances instances = instancesTrainingMap.get(seaFoodId);
//		logger.info("weight of 0: " + instances.attribute(0).weight());
//		logger.info("weight of 1: " + instances.attribute(1).weight());
		logger.info("prepare instance: ...");
		Instance instance = new Instance(instances.numAttributes() - 1);
		instance.setDataset(instances);
		logger.info("instance numAtt: " + instance.numAttributes());
//		for (int i = 0; i < symptomValueIds.size(); i++) {
//			instance.setValue(i, symptomValueIds.get(i));
//		}
		for (String value : symptomValueIds) {
			String symptomId = getSymptomIdFromValue(Long.valueOf(value));
			if (Long.valueOf(value) > 0 && Arrays.asList(ConstraintUtil.weakFeats).contains(symptomId) == false) {
				instance.setValue(instances.attribute(getSymptomIdFromValue(Long.valueOf(value))).index(), value);
			}
		}
		return instance;
	}

	private Attribute createSymptomAtt(Symptom symptom) {
		FastVector attVec = new FastVector();
		List<SymptomValue> valueList = symptom.getSymptomValues();
		for (SymptomValue value : valueList) {
			attVec.addElement(value.getId().toString());
		}
		Attribute att = new Attribute(symptom.getId().toString(), attVec);
		if (Arrays.asList(ConstraintUtil.weakFeats).contains(String.valueOf(symptom.getId()))) {
			att.setWeight(0.3);
		} else if (Arrays.asList(ConstraintUtil.intermediateFeats).contains(String.valueOf(symptom.getId()))) {
			att.setWeight(1);
		} else if (Arrays.asList(ConstraintUtil.strongFeats).contains(String.valueOf(symptom.getId()))) {
			att.setWeight(5);
		}
		return att;
	}

	private Attribute createLabelAtt(SeaFood seaFood) {
		FastVector labelVec = new FastVector();
		List<Diasease> diaseases = seaFood.getDiaseases();
		for (Diasease diasease : diaseases) {
			labelVec.addElement(diasease.getId().toString());
		}
		return new Attribute("label", labelVec);

	}

	private String convertToFileName(int seaFoodId, int dataType) {
		if (dataType == 1) {
			return ConstraintUtil.TRAINING_DIR + "training_" + seaFoodId + ".arff";
		} else if (dataType == 2) {
			return ConstraintUtil.TESTING_DIR + "testing_" + seaFoodId + ".arff";
		} else {
			return ConstraintUtil.ORIGIN_DIR + "origin_" + seaFoodId + ".arff";
		}

	}

	private RandomForest createClassifier(Instances instances) {
		try {
			RandomForest classifier = new RandomForest();
			classifier.setNumTrees(30);
//			classifier.setNumFeatures(10);
			classifier.buildClassifier(instances);
			return classifier;
		} catch (Exception e) {
			e.printStackTrace();
			return new RandomForest();
		}
	}

	private RandomForest getClassifier(int seaFoodId) {
		return this.classifierMap.get(seaFoodId);
	}

	private Id3 getId3Tree(int seaFoodId) {
		return this.id3SuggestingTree.get(seaFoodId);
	}

	private Instances getTrainingInstances(int seaFoodId, boolean isTraining) {
		return instancesTrainingMap.get(seaFoodId);
	}

	public Map<Integer, RandomForest> getClassifierMap() {
		return classifierMap;
	}

	public void setClassifierMap(Map<Integer, RandomForest> classifierMap) {
		this.classifierMap = classifierMap;
	}
}