package com.datn.service.classifier;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IClassifierService {
//	public String predict(int seaFoodId, List<String> symptomValueIds);
	public void loadDataOrCreateIfNotExisted();
	public void forcusRenewDataAndArff();
	public Optional<String> getId3Suggesting(int seaFoodId);
	public Map<String, Double> getTopResultWithPercentage(int number, int seaFoodId, List<String> symptomValueIds);
}
