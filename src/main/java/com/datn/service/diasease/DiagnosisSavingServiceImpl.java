package com.datn.service.diasease;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.diasease.DiagnosisSaving;
import com.datn.repository.diasease.DiagnosisSavingRepository;

@Service
public class DiagnosisSavingServiceImpl implements IDiagnosisSavingService {
	@Autowired
	private DiagnosisSavingRepository repo;

	@Override
	public DiagnosisSaving save(DiagnosisSaving obj) {
		return repo.save(obj);
	}

	@Override
	public List<DiagnosisSaving> getByUserId(Long userId) {
		return repo.getSavingByUserId(userId);
	}

	@Override
	public Optional<DiagnosisSaving> getById(Integer id) {
		return repo.findById(id);
	}

	@Override
	public void delete(DiagnosisSaving save) {
		repo.delete(save);
		
	}
	
	
	
	
}
