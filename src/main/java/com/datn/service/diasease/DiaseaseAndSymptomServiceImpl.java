package com.datn.service.diasease;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.diasease.DiaseaseAndSymptom;
import com.datn.repository.diasease.DiaseaseAndSymptomRepository;

@Service
public class DiaseaseAndSymptomServiceImpl implements IDiaseaseAndSymptomService {

	@Autowired
	private DiaseaseAndSymptomRepository diaseaseAndSymptomRepo;
	
	@Override
	public List<DiaseaseAndSymptom> getBySeaFoodId(int seaFoodId) {
		return diaseaseAndSymptomRepo.getBySeaFoodId(seaFoodId);
	}

}
