package com.datn.service.review;

import java.util.List;
import java.util.Optional;

import com.datn.entity.review.LikeReview;

public interface ILikeReviewService {
	public LikeReview create(LikeReview likeReview);
	public void delete(LikeReview likeReview);
	public Optional<LikeReview> getById(Integer id);
	public Optional<LikeReview> getByReviewAndUser(Integer reviewId, Long userId);
	public List<LikeReview> getByDiaseaseAndUser(Integer diaseaseId, Long userId);
}
