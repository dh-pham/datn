package com.datn.service.seafood;

import java.util.List;
import java.util.Optional;

import com.datn.entity.SeaFood;
import com.datn.entity.symptom.SymptomValue;

public interface ISeaFoodService {
	public List<SeaFood> getAll();
	public Optional<SeaFood> getById(Integer id);
	public List<SymptomValue> getSymptomValues(Integer seaFoodId, Integer symptomId);
}
