package com.datn.service.seafood;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.SeaFood;
import com.datn.entity.symptom.SymptomValue;
import com.datn.repository.SeaFoodRepository;

@Transactional
@Service
public class SeaFoodServiceImpl implements ISeaFoodService{
	@Autowired
	private SeaFoodRepository seaFoodRepo;
	@Override
	public List<SeaFood> getAll() {
		return seaFoodRepo.findAll();
	}
	@Override
	public Optional<SeaFood> getById(Integer id) {
		return seaFoodRepo.findById(id);
	}
	@Override
	public List<SymptomValue> getSymptomValues(Integer seaFoodId, Integer symptomId) {
		return seaFoodRepo.getSymptomValues(seaFoodId, symptomId);
	}

}
