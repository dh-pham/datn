package com.datn.service.symptom;

import java.util.List;

import com.datn.entity.symptom.SymptomType;

public interface ISymptomTypeService {
	public List<SymptomType> getAll();
}
