package com.datn.service.symptom;

import java.util.List;

import com.datn.entity.symptom.SymptomValue;

public interface ISymptomValueService {
	public List<SymptomValue> getAll();
	public List<SymptomValue> getPopularSymptomValueForAnDiasease(Integer diaseaseId);
	public List<SymptomValue> getBySymptomId(Integer symptomId);
}
