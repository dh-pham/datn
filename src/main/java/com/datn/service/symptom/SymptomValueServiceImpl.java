package com.datn.service.symptom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datn.entity.symptom.SymptomValue;
import com.datn.repository.symptom.SymptomValueRepository;

@Service
public class SymptomValueServiceImpl implements ISymptomValueService{
	@Autowired
	private SymptomValueRepository symptomValueRepo;
	@Override
	public List<SymptomValue> getAll() {
		return symptomValueRepo.findAll();
	}
	@Override
	public List<SymptomValue> getPopularSymptomValueForAnDiasease(Integer diaseaseId) {
		return symptomValueRepo.getPopularSymptomValueForAnDiasease(diaseaseId);
	}
	
	@Override
	public List<SymptomValue> getBySymptomId(Integer symptomId) {
		return symptomValueRepo.getBySymptomId(symptomId);
	}
	
}
