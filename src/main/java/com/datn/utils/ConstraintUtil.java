package com.datn.utils;

public class ConstraintUtil {

	public static final String DATA_DIR = "./data/";
	public static final String TRAINING_DIR = "./data/trainingSet/";
	public static final String TESTING_DIR = "./data/testingSet/";
	public static final String ORIGIN_DIR = "./data/originSet/";
	public static final Integer MIN = -99999999;
	public static final String[] weakFeats= {"1", "6", "15", "16", "17"};
	public static final String[] intermediateFeats= {"2", "3", "8"};
	public static final String[] strongFeats = {"7", "9", "4", "5", "10", "11", "12", "13", "14"};
}
