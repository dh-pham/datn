package com.datn.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OtherUtil {

	public static void deleteDirectoryRecursion(File file) throws IOException {
		if (file.isDirectory()) {
			File[] entries = file.listFiles();
			if (entries != null) {
				for (File entry : entries) {
					deleteDirectoryRecursion(entry);
				}
			}
		}
		if (!file.delete()) {
			throw new IOException("Failed to delete " + file);
		}
	}
	
	public static Integer getIndexOfMaxEle(List<Double> dbList, List<Integer> indexList) {
		if (dbList.size() < 1) return -1;
		double max = (double) ConstraintUtil.MIN;
		int maxIndex = -1;
		for (int i = 0; i < dbList.size(); i++) {
			if (indexList.contains(i) == false) {
				if (max < dbList.get(i)) {
					max = dbList.get(i);
					maxIndex = i;
				}
			}
			
		}
		return maxIndex;
	}
	
	public static List<Integer> getIndexsOfTopMaxEle(int number, double[] dbs) {
		List<Double> dbList = Arrays.stream(dbs).boxed().collect(Collectors.toList());
		List<Integer> indexList = new ArrayList<>();
		if (number > dbs.length) number = dbs.length;
		for (int i = 0; i < number; i++) {
			int maxIndex = getIndexOfMaxEle(dbList, indexList);
			indexList.add(maxIndex);
		}
		return indexList;
	}
}
