package com.datn.utils.auth;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncryptedPasswordUtil {
 
    // Encrypt Password with BCryptPasswordEncoder
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }
    
    public static boolean matches(String rawPassword, String encodedPassword) {
    	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    	return encoder.matches(rawPassword, encodedPassword);
    }

     
}