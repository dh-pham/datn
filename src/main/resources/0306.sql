-- MySQL dump 10.13  Distrib 8.0.16, for Linux (x86_64)
--
-- Host: localhost    Database: db_hpd
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `diagnosisSaving`
--

DROP TABLE IF EXISTS `diagnosisSaving`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `diagnosisSaving` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `symptoms` varchar(1200) DEFAULT NULL,
  `diaseases` varchar(500) DEFAULT NULL,
  `savedTime` timestamp NULL DEFAULT NULL,
  `seaFoodId` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_fk_1` (`userId`),
  KEY `seaFood_fk_2` (`seaFoodId`),
  CONSTRAINT `seaFood_fk_2` FOREIGN KEY (`seaFoodId`) REFERENCES `seaFood` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_fk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnosisSaving`
--

LOCK TABLES `diagnosisSaving` WRITE;
/*!40000 ALTER TABLE `diagnosisSaving` DISABLE KEYS */;
/*!40000 ALTER TABLE `diagnosisSaving` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diasease`
--

DROP TABLE IF EXISTS `diasease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `diasease` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `seaFoodId` smallint(6) NOT NULL,
  `diaseaseTypeId` tinyint(4) NOT NULL DEFAULT '-1',
  `diaseaseCode` varchar(20) NOT NULL,
  `diaseaseName` varchar(200) NOT NULL,
  `treatment` varchar(2000) NOT NULL,
  `treatmentFile` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `scoreAvg` float(3,2) DEFAULT '0.00',
  `reviewerAmount` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FKc5g2a8h4ms7yv2yk370hia6t4` (`diaseaseTypeId`),
  KEY `FK2t0wtyjck5f3r9d1gd2srpq5m` (`seaFoodId`),
  CONSTRAINT `FK2t0wtyjck5f3r9d1gd2srpq5m` FOREIGN KEY (`seaFoodId`) REFERENCES `seaFood` (`id`),
  CONSTRAINT `FKc5g2a8h4ms7yv2yk370hia6t4` FOREIGN KEY (`diaseaseTypeId`) REFERENCES `diaseaseType` (`id`),
  CONSTRAINT `benh_ibfk_1` FOREIGN KEY (`seaFoodId`) REFERENCES `seaFood` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `benh_ibfk_2` FOREIGN KEY (`diaseaseTypeId`) REFERENCES `diaseaseType` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diasease`
--

LOCK TABLES `diasease` WRITE;
/*!40000 ALTER TABLE `diasease` DISABLE KEYS */;
INSERT INTO `diasease` VALUES (1,2,1,'BENHT0HK0F','Bệnh rận cá- Caligosis','Dùng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Dùng lá xoan băm nhỏ bón xuống ao, lieuf lượng 0,3-0,5kg/m3 nước; Hoặc dùng thuốc chiết xuất từ lá xon, liều dùng theo nhà sản xuất.W35','NULL','/img/diaseases/1.png',NULL,0),(2,1,3,'BENHPDG2EJ',' Bệnh đốm đỏ, viêm ruột do vi khuẩn ',' Không cho cá ăn thức ăn hư hỏng; Định kỳ 10-15 ngày khử trùng môi trường bằng VICATO, liều lượng 0,5-0,7g/m3 nước. Cải thiện môi trường nuôi bằng vôi, liều lượng 20-30kg/1000m3 nước, định kỳ 15 ngày 1 lần. Định kỳ 1 tháng cho cá ăn 3 ngày liên tục thuốc KN - 04 - 12, liều lượng 2g/1kg cá/ngày; Hoặc dùng tỏi tươi 0,5kg/100kg cá/ngày để phòng bệnh. Cá bị bệnh cho ăn 7-10 ngày liên tục. Định kỳ 1 tháng cho cá ăn 7 ngày liên tục Vitamin C, liều lượng 2-3g/100kg cá/ngày. ','NULL','/img/diaseases/2.png',5.00,1),(3,1,1,'BENHXZ1WCL',' Bệnh nấm thuỷ mi ',' Dùng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. W43','NULL','/img/diaseases/3.png',0.00,0),(4,1,3,'BENHTAF5KQ','Bệnh sán dây ','Dùng thuốc Praziquantel hoặc Mebendazole để tẩy  sán dây, cho cá ăn  là 75mg thuốc/kg cá/ngày với liệu trình 3 ngày liên tục. Định kỳ 1-2 tháng cho ăn 1 đợt.','NULL','/img/diaseases/4.png',0.00,0),(5,1,1,'BENHDCIMIQ',' Bệnh trùng mỏ neo',' Dùng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Dùng lá xoan băm nhỏ bón xuống ao, lieuf lượng 0,3-0,5kg/m3 nước; Hoặc dùng thuốc chiết xuất từ lá xon, liều dùng theo nhà sản xuất.W35','NULL','/img/diaseases/5.png',0.00,0),(6,4,1,'BENH2DXNBN',' Bệnh vi rút KHV',' Định kỳ 10-15 ngày khử trùng môi trường bằng VICATO, liều lượng 0,5-0,7g/m3 nước. Cải thiện môi trường nuôi bằng vôi, liều lượng 20-30kg/1000m3 nước, định kỳ 15 ngày 1 lần. Định kỳ 1 tháng cho cá ăn 3 ngày liên tục thuốc KN - 04 - 12, liều lượng 2g/1kg cá/ngày; Hoặc dùng tỏi tươi 0,5kg/100kg cá/ngày để phòng bệnh. Cá bị bệnh cho ăn 7-10 ngày liên tục. Định kỳ 1 tháng cho cá ăn 7 ngày liên tục Vitamin C, liều lượng 2-3g/100kg cá/ngày. Dùng vacxin phòng bệnh Reovirus.','NULL','/img/diaseases/6.png',0.00,0),(7,4,3,'BENH1QCYL4','Bệnh bào tử sợi 1 cực nang- Thelohanellosis trong xoang bụng và ruột','Bào tử sợi có lớp vỏ rất dày, đến nay chưa có thuốc hữu hiệu diệt bào tử sợi nhưng không ảnh hưởng đến cá. Áp dụng biện pháp phòng bệnh tổng hợp: Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/7.png',0.00,0),(8,4,1,'BENH7JWWXY',' Bệnh trùng bánh xe',' Tắm cho cá bằng sunphat đồng (CuSO4), nồng độ 3-5g/m3 nước, thời gian 10-15 phút, sau cho cá nước sạch; Hoặc phun trực tiếp xuống ao nồng độ 0,5-0,7g/m3 nước. 1 tuần tắm hoặc phun 2-3 lần là cá khỏi bệnh','NULL','/img/diaseases/8.png',0.00,0),(9,4,3,'BENHBFBJ3C',' bệnh sán lá đơn chủ đẻ trứng',' Dùng Formalin tắm cho cá liều lượng 100-200ml/m3 nước, thời gian 30-60 phút, chuyển cá ra nước sạch; Hoặc phun xuống ao, liều lượng 10-20ml/m3 nước. Tắm  NH4OH, liều lượng 1g/m3 (1ppm), thời gian 5 phút chuyến cá ra nước sạch. Dùng một số thảo dược diệt được giun sán như keo dâu, hạt cao.','NULL','/img/diaseases/9.png',0.00,0),(10,6,5,'BENH9DZB9L',' Bệnh trùng bào tử sợi có 2 cực nang',' Bào tử sợi có lớp vỏ rất dày, đến nay chưa có thuốc hữu hiệu diệt bào tử sợi nhưng không ảnh hưởng đến cá. Áp dụng biện pháp phòng bệnh tổng hợp: Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/10.png',0.00,0),(11,6,3,'BENH2NJ9X5','Bệnh trùng lông nội ký sinh = Balantidiosis','Áp dụng theo biện pháp phòng bệnh tổng hợp','NULL','/img/diaseases/11.png',0.00,0),(12,6,3,'BENHAM4MEH',' Bệnh trùng lông nội ký sinh = Ichthyonyctosis',' Áp dụng theo biện pháp phòng bệnh tổng hợp','NULL','/img/diaseases/12.png',0.00,0),(14,2,3,'BENHQFFPKY',' Bệnh sán lá đơn chủ đẻ con',' Xem bệnh sán lá đơn chủ cá tra','NULL','/img/diaseases/14.png',0.00,0),(15,1,1,'BENHYKY84R','Bệnh xuất huyết do vi rút','Định kỳ 10-15 ngày khử trùng môi trường bằng VICATO, liều lượng 0,5-0,7g/m3 nước. Cải thiện môi trường nuôi bằng vôi, liều lượng 20-30kg/1000m3 nước, định kỳ 15 ngày 1 lần. Định kỳ 1 tháng cho cá ăn 3 ngày liên tục thuốc KN - 04 - 12, liều lượng 2g/1kg cá/ngày; Hoặc dùng tỏi tươi 0,5kg/100kg cá/ngày để phòng bệnh. Cá bị bệnh cho ăn 7-10 ngày liên tục. Định kỳ 1 tháng cho cá ăn 7 ngày liên tục Vitamin C, liều lượng 2-3g/100kg cá/ngày. Dùng vacxin phòng bệnh Reovirus.','NULL','/img/diaseases/15.png',0.00,0),(16,6,1,'BENHRP8ZQ2',' Bệnh trùng quả dưa = Ichthyophthyriosis',' Dùng Formalin tắm cho cá liều lượng 100-200ml/m3 nước, thời gian 30-60 phút, chuyển cá ra nước sạch; Hoặc phun xuống ao, liều lượng 10-20ml/m3 nước. ','NULL','/img/diaseases/16.png',0.00,0),(17,4,2,'BENHNBYIJD',' Bệnh bào tử sợi 1 cực nang- Thelohanellosis trên vây và mang',' Bào tử sợi có lớp vỏ rất dày, đến nay chưa có thuốc hữu hiệu diệt bào tử sợi nhưng không ảnh hưởng đến cá. Áp dụng biện pháp phòng bệnh tổng hợp: Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/17.png',0.00,0),(18,1,2,'BENHDDPXM9','Thối mang do vi khuẩn','Không cho cá ăn thức ăn hư hỏng; Định kỳ 10-15 ngày khử trùng môi trường bằng VICATO, liều lượng 0,5-0,7g/m3 nước. Cải thiện môi trường nuôi bằng vôi, liều lượng 20-30kg/1000m3 nước, định kỳ 15 ngày 1 lần. Định kỳ 1 tháng cho cá ăn 3 ngày liên tục thuốc KN - 04 - 12, liều lượng 2g/1kg cá/ngày; Hoặc dùng tỏi tươi 0,5kg/100kg cá/ngày để phòng bệnh. Cá bị bệnh cho ăn 7-10 ngày liên tục. Định kỳ 1 tháng cho cá ăn 7 ngày liên tục Vitamin C, liều lượng 2-3g/100kg cá/ngày. ','NULL','/img/diaseases/18.png',0.00,0),(19,1,2,'BENHGYVSA1',' Bệnh giáp xác chân chèo',' Dùng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Dùng lá xoan băm nhỏ bón xuống ao, lieuf lượng 0,3-0,5kg/m3 nước; Hoặc dùng thuốc chiết xuất từ lá xon, liều dùng theo nhà sản xuất.W35','NULL','/img/diaseases/19.png',0.00,0),(20,1,1,'BENHKKXRWR',' Bệnh rận cá',' Dùng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Dùng lá xoan băm nhỏ bón xuống ao, lieuf lượng 0,3-0,5kg/m3 nước; Hoặc dùng thuốc chiết xuất từ lá xon, liều dùng theo nhà sản xuất.W35','NULL','/img/diaseases/20.png',0.00,0),(21,4,2,'BENH38ZV1N',' Bệnh bào tử sợi 2 cực nang- Myxobolosis trên mang',' Bào tử sợi có lớp vỏ rất dày, đến nay chưa có thuốc hữu hiệu diệt bào tử sợi nhưng không ảnh hưởng đến cá. Áp dụng biện pháp phòng bệnh tổng hợp: Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/21.png',0.00,0),(22,6,1,'BENHYUERXZ',' Bệnh trùng loa kèn',' Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho cá ăn thuốc kháng sinh Doxycyclin, liều dùng 100mg/1kg cá/ngày đầu; từ ngày thứ 2 đến 7 cho ăn 50mg/1kg cá/ngày hoặc Sulfatrim, liều lượng 200mg/1kg cá/ngày đầu, từ ngày thứ 2-7 cho ăn 100mg/1kg cá/ngày. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/22.png',0.00,0),(23,6,1,'BENHHYOTNT',' Bệnh trùng bánh xe',' Tắm cho cá bằng sunphat đồng (CuSO4), nồng độ 3-5g/m3 nước, thời gian 10-15 phút, sau cho cá nước sạch; Hoặc phun trực tiếp xuống ao nồng độ 0,5-0,7g/m3 nước. 1 tuần tắm hoặc phun 2-3 lần là cá khỏi bệnh','NULL','/img/diaseases/23.png',0.00,0),(24,6,1,'BENH3CVAFW',' Bệnh giun tròn trong cuống mật-Cucullanus chabaudi',' Dùng thuốc Praziquantel hoặc Mebendazole để tẩy  giun tròn, cho cá ăn  là 75mg thuốc/kg cá/ngày với liệu trình 3 ngày liên tục. Định kỳ 1-2 tháng cho ăn 1 đợt.','NULL','/img/diaseases/24.png',0.00,0),(25,6,2,'BENH9AOVPE',' Bệnh sán lá đơn chủ đẻ trứng',' Dùng Formalin tắm cho cá liều lượng 100-200ml/m3 nước, thời gian 30-60 phút, chuyển cá ra nước sạch; Hoặc phun xuống ao, liều lượng 10-20ml/m3 nước. Tắm  NH4OH, liều lượng 1g/m3 (1ppm), thời gian 5 phút chuyến cá ra nước sạch. Dùng một số thảo dược diệt được giun sán như keo dâu, hạt cao.','NULL','/img/diaseases/25.png',0.00,0),(26,2,1,'BENHH8YLR1',' Bệnh trùng quả dưa',' Xem bệnh trùng quả dưa của cá tra','NULL','/img/diaseases/26.png',0.00,0),(27,1,1,'BENHXVBFDL','Bệnh trùng bánh xe','Tắm cho cá bằng sunphat đồng (CuSO4), nồng độ 3-5g/m3 nước, thời gian 10-15 phút, sau cho cá nước sạch; Hoặc phun trực tiếp xuống ao nồng độ 0,5-0,7g/m3 nước. 1 tuần tắm hoặc phun 2-3 lần là cá khỏi bệnh','NULL','/img/diaseases/27.png',0.00,0),(28,4,1,'BENHMXF9SQ','Trùng mỏ neo','Dùng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Dùng lá xoan băm nhỏ bón xuống ao, lieuf lượng 0,3-0,5kg/m3 nước; Hoặc dùng thuốc chiết xuất từ lá xon, liều dùng theo nhà sản xuất.W35','NULL','/img/diaseases/28.png',0.00,0),(29,6,1,'BENHFAWDCZ','Bênh xuất huyết hoại tử (đốm đỏ)','Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho cá ăn thuốc kháng sinh Florfenicol hoặc Doxycyclin, liều dùng 100mg/1kg cá/ngày đầu; từ ngày thứ 2 đến 7 cho ăn 50mg/1kg cá/ngày. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/29.png',0.00,0),(30,1,1,'BENHGQ3CJ3',' Bệnh trùng lông- Balantidiosis',' Áp dụng theo biện pháp phòng bệnh tổng hợp','NULL','/img/diaseases/30.png',0.00,0),(31,4,5,'BENHAGTIB6',' Bệnh viêm bóng hơi (Bệnh xuất huyết mùa xuân)','Định kỳ 10-15 ngày khử trùng môi trường bằng VICATO, liều lượng 0,5-0,7g/m3 nước. Cải thiện môi trường nuôi bằng vôi, liều lượng 20-30kg/1000m3 nước, định kỳ 15 ngày 1 lần. Định kỳ 1 tháng cho cá ăn 3 ngày liên tục thuốc KN - 04 - 12, liều lượng 2g/1kg cá/ngày; Hoặc dùng tỏi tươi 0,5kg/100kg cá/ngày để phòng bệnh. Cá bị bệnh cho ăn 7-10 ngày liên tục. Định kỳ 1 tháng cho cá ăn 7 ngày liên tục Vitamin C, liều lượng 2-3g/100kg cá/ngày. Dùng vacxin phòng bệnh Reovirus.','NULL','/img/diaseases/31.png',0.00,0),(32,6,1,'BENHUI5NFZ','Bệnh xuất huyết do vi khuẩn Aeromonas di động',' Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho cá ăn thuốc kháng sinh Doxycyclin, liều dùng 100mg/1kg cá/ngày đầu; từ ngày thứ 2 đến 7 cho ăn 50mg/1kg cá/ngày hoặc Sulfatrim, liều lượng 200mg/1kg cá/ngày đầu, từ ngày thứ 2-7 cho ăn 100mg/1kg cá/ngày. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/32.png',0.00,0),(33,2,5,'BENHNTHVQY','Hội chứng bơi quay tròn',' Định kỳ 10-15 ngày khử trùng môi trường bằng VICATO, liều lượng 0,5-0,7g/m3 nước. Cải thiện môi trường nuôi bằng vôi, liều lượng 20-30kg/1000m3 nước, định kỳ 15 ngày 1 lần. Định kỳ 1 tháng cho cá ăn 3 ngày liên tục thuốc KN - 04 - 12, liều lượng 2g/1kg cá/ngày; Hoặc dùng tỏi tươi 0,5kg/100kg cá/ngày để phòng bệnh. Cá bị bệnh cho ăn 7-10 ngày liên tục. Định kỳ 1 tháng cho cá ăn 7 ngày liên tục Vitamin C, liều lượng 2-3g/100kg cá/ngày. Dùng vacxin phòng bệnh Reovirus.','NULL','/img/diaseases/33.png',0.00,0),(34,2,5,'BENHHHVO4F','Bệnh xuất huyết do liên cầu khuẩn','Định kỳ10-15 ngày 1 lần  khử trùng nước bằng VICATO, liều dùng 0,5-0,7g/m3 nước; Hoặc Povidne 90%, liều dùng 0,5ml/m3 nước. Cho cá ăn thuốc kháng sinh Florfenicol hoặc Doxycyclin, liều dùng 100mg/1kg cá/ngày đầu; từ ngày thứ 2 đến 7 cho ăn 50mg/1kg cá/ngày. Cho ăn Vitamin C, liều lượng 2-3g/100kg cá/ngày, 1 tháng cho ăn 7 ngày liên tục.','NULL','/img/diaseases/34.png',0.00,0),(35,1,1,'NULL','Bệnh trùng loa kèn cá trắm cỏ','NULL','NULL','/img/diaseases/default.png',0.00,0),(36,1,2,'NULL','Bệnh nấm mang','NULL','NULL','/img/diaseases/default.png',0.00,0),(37,1,1,'NULL','Bệnh trùng roi-Cryptobiosis','NULL','NULL','/img/diaseases/default.png',0.00,0),(38,1,2,'NULL',' Bệnh trùng\n roi-Ichthyobodosis','NULL','NULL','/img/diaseases/default.png',0.00,0),(39,1,2,'NULL','Bệnh sán lá đơn chủ \n16 móc - Dactylogyrosis','NULL','NULL','/img/diaseases/default.png',0.00,0),(40,1,3,'NULL','Bệnh sán lá đơn chủ 18 móc - Gyrodactylosis','NULL','NULL','/img/diaseases/default.png',0.00,0),(41,1,3,'NULL','Bệnh ấu trùng sán lá sang chủ ở mang - Centrocestosis','NULL','NULL','/img/diaseases/default.png',0.00,0),(42,1,1,'NULL','Bệnh rận cá - Argulosis','Diệt trứng và ấu trùng cần tát cạn ao, dọn sạch đáy, dùng vôi tẩy ao và phơi khô đáy ao trước khi thả cá vào nuôi. Nuôi cá lồng thường xuyên treo túi vôi liều lượng 2-4kg/10m³ lồng.\n\nĐể trị bệnh có thể dùng thuốc tím tắm cho cá bệnh nồng độ 10 ppm trong thời gian 30 phút, hoặc nước muối 2-4%.\n\nDùng Seaweed 2-2.5 lít/1.000 m³ nước, mỗi tuần xử lý một lần, trong 2 tuần','','/img/diaseases/default.png',0.00,0),(43,1,1,'NULL','Bệnh rận cá - Corallanosis','NULL','NULL','/img/diaseases/default.png',0.00,0),(44,7,-1,'NULL','Bệnh MBV tôm sú\n','- Đường lay nhiễm bệnh chính là từ nguồn giống, kế tiếp là chất lượng môi trường nước của ao, đầm nuôi tôm không đảm bảo. Tôm bị nhiễm (tôm còi) sẽ lây lan đến tôm khoẻ nếu nuôi chung trong một ao. Vậy phải chọn giống khỏe, không nhiễm MBV và HPV, luôn vệ sinh ao đảm bảo chất lượng và chăm sóc quản lý tốt sức khỏe tôm.\n \n- Loại bỏ tôm bệnh: dùng những bó chà nhỏ cắm quanh ao trong 1 -2 tháng đầu, tôm nhỏ, yếu sẽ bám vào chà, khi kiểnm tra thì bỏ những tôm này ra khỏi ao.\n \n- Sau 2 tháng nuôi, cặn bả tập trung vào giữa ao và tôm nhỏ yếu thường tập trung vào vùng dơ bẩn này, nên rải thức ăn cho tôm từ trong ra ngoài theo hình xoắn ốc để kích thích tôm hướng ra ngoài.','NULL','/img/diaseases/default.png',0.00,0),(45,11,-1,'NULL','Bệnh vi rút đốm trắng','NULL','NULL','/img/diaseases/default.png',0.00,0),(46,7,-1,'NULL','Bệnh đầu vàng tôm sú','Áp dụng theo phương pháp phòng bệnh tổng hợp. Tránh vận chuyển tôm từ nơi có bệnh đến nơi chưa phát bệnh để hạn chế sự lây lan vùng lân cận. Những tôm chết vớt ra khỏi ao, tốt nhất là chôn sống trong vôi nung hoặc đốt. Nước từ ao tôm bệnh không thải ra ngoài xử lý bằng vôi nung hoặc bằng clorua vôi (theo phương pháp tẩy ao). Xem xét tôm thường xuyên, nếu phát hiện có dấu hiệu bệnh, tốt nhất là thu hoạch ngay. Nếu tôm quá nhỏ không đáng thu hoạch thì cần xử lý nước ao trước khi tháo bỏ.','NULL','/img/diaseases/default.png',0.00,0),(47,7,-1,'NULL','Bệnh nhiễm trùng virus dưới da và hoại tử ở tôm sú\n','NULL','NULL','/img/diaseases/default.png',0.00,0),(48,8,-1,'NULL','Bệnh nhiễm trùng virus dưới da và hoại tử ở tôm chân trắng\n','NULL','NULL','/img/diaseases/default.png',0.00,0),(49,9,-1,'NULL','Bệnh Parvovirus gan tuỵ','NULL','NULL','/img/diaseases/default.png',0.00,0),(50,8,-1,'NULL','Bệnh hoại tử cơ (đục thân) do virut ở tôm chân trắng\n','NULL','NULL','/img/diaseases/default.png',0.00,0),(51,7,-1,'NULL','Bệnh hoại tử cơ (đục thân) do virut ở tôm sú\n','NULL','NULL','/img/diaseases/default.png',0.00,0),(52,8,-1,'NULL','Bệnh đuôi đỏ','NULL','NULL','/img/diaseases/default.png',0.00,0),(53,9,-1,'NULL','Bệnh Virus hoại tử tuyến ruột giữa','- Áp dụng biện pháp phòng bệnh chung.','NULL','/img/diaseases/default.png',0.00,0),(54,10,-1,'NULL','Bệnh đuôi trắng tôm càng xanh','Hiện chưa có phương pháp điều trị bệnh này. Để có thể ngăn ngừa cần phải áp dụng các biện pháp quản lí tốt trang trại để tránh thiệt hại.','NULL','/img/diaseases/default.png',0.00,0),(55,11,-1,'NULL','Bệnh sữa do Rickettsia ở tôm hùm\n','Khi tôm mắc bệnh, chỉ điều trị tôm hùm bị bệnh nhẹ, khi dịch bệnh mới xuất hiện để hạn chế lây lan. Tùy điều kiện cụ thể để áp dụng các phác đồ điều trị bệnh cho tôm. Người nuôi có thể tham khảo phác đồ điều trị sau: Treo túi thuốc khử trùng, có thể sử dụng Chlorine Dioxide (thành phần chính là Natri Chlorite, NaClO2), mỗi lồng 02 túi, mỗi túi 10 viên (10g thuốc), 1 lần/ngày. Dùng Doxycyclin 10% trộn thức ăn với lượng 7g (khoảng 2 muỗng cafe)/kg thức ăn (lựa chọn loại thức ăn tôm hùm ưa thích, kích cỡ thức ăn phù hợp với kích cỡ miệng tôm, sau khi trộn thuốc phải có thời gian để thuốc ngấm), áp dụng 1 lần/ngày và thực hiện trong 7 ngày liên tục. Lượng thức ăn trộn thuốc nên sử dụng với lượng ít hơn bình thường để tôm sử dụng hết thức ăn, sau đó điều chỉnh tăng dần cho phù hợp. Bổ sung Premix (vitamin, axit amin, khoáng chất): trộn thức ăn trong toàn bộ quá trình điều trị. Thời gian điều trị khoảng 10 ngày, sau 10 ngày thì dừng thuốc hoàn toàn, nếu không khỏi chuyển sang phác đồ tiêm. Kỹ thuật trộn thức ăn, cho ăn theo hướng dẫn của Cơ quan quản lý thú y hoặc Chi cục Thủy sản.','NULL','/img/diaseases/default.png',0.00,0),(56,7,-1,'NULL','Bệnh do vi khuẩn Vibrio ở tôm sú\n','Các trại sản xuất tôm cần thực hiện một số biện pháp để phòng bệnh do vi khuẩn Vibrio như: lọc nước qua tầng lọc cát và xử lý tia cực tím, xử lý tôm bố mẹ bằng Formalin, xử lý Artemia bằng Chlorin ở nước ngọt, vớt ra rửa sạch rồi mới cho ấp, thường xuyên xi phông đáy để giảm lượng vi khuẩn ở tầng đáy bể ương... Trường hợp bị bệnh nặng phải hủy đợt sản xuất và xử lý bằng Chlorin rồi mới xả ra ngoài.','NULL','/img/diaseases/default.png',0.00,0),(57,11,5,'NULL','Bệnh hoại tử gan tụy cấp','NULL','NULL','/img/diaseases/default.png',0.00,0),(58,10,-1,'NULL','Bệnh đục cơ tôm càng xanh\n','Đến thời điểm hiện tại, bệnh này vẫn chưa có thuốc đặc trị mà mới chỉ có các biện pháp ngăn ngừa bệnh bùng phát và lây lan diện rộng. Vì thế, bà con cần phải thực hiện phòng bệnh ngay từ ban đầu theo các bước sau:\n– Chọn tôm bố mẹ khỏe mạnh, linh hoạt, không nhiễm bệnh, màu sắc sáng.\n– Chọn tôm giống thả nuôi có kích cỡ đồng đều, cơ không bị đục, màu sắc sáng, bơi lội linh hoặt,…\n– Quản lý môi trường nước ổn định, bón vôi (CaCO3) để tăng độ pH cho tôm.\n– Bổ sung thêm vitamin C, khoáng chất thiết yếu hay những chất kích \nthích giúp tôm ăn khỏe, tăng trưởng nhanh. Sử dụng các loại chế phẩm vi \nsinh tự nhiên như EMS-Proof, Gut-Well để bổ sung lợi khuẩn ức chế vi khuẩn gậy hại phát triển.\n– Thường xuyên vệ sinh, khử trùng trại và các loại dụng cụ sản xuất, \nxử lý nước cấp và nước thải, đồng thời thực hành quản lý tốt ở trại \ngiống và ao nuôi để góp phần giảm thiểu sự lây lan mầm bệnh. Sử dụng các\n loại chế phẩm vi sinh xử lý môi trường ao nuôi như: Bottom-Up, Bac-Up, Sober – Up,…','NULL','/img/diaseases/default.png',0.00,0),(59,10,-1,'NULL','Bệnh đốm nâu','Cách phòng bệnh là nên chú ý đến nguồn nước trong ao, cần phải thay nước cho ao thường xuyên và trong ao  nên thả tôm với mật độ vừa phải. Ngoài ra, ta nên cho tôm ăn bổ dưỡng hơn, để tôm có sức đề kháng chống chọi lại bệnh. Hiện nay, chưa có thuốc đặc trị về bệnh này.','NULL','/img/diaseases/default.png',0.00,0),(60,11,-1,'NULL','Bệnh vi khuẩn dạng sợi','NULL','NULL','/img/diaseases/default.png',0.00,0),(61,7,-1,'NULL','Bệnh đốm trắng do vi khuẩn','NULL','NULL','/img/diaseases/default.png',0.00,0),(62,11,-1,'NULL','Bệnh nấm ở tôm','NULL','NULL','/img/diaseases/default.png',0.00,0),(63,11,-1,'NULL','Bệnh trùng hai tế bào','NULL','NULL','/img/diaseases/default.png',0.00,0),(64,7,-1,'NULL','Bệnh tôm bông trên tôm sú','NULL','NULL','/img/diaseases/default.png',0.00,0),(65,9,-1,'NULL','Bệnh tôm bông trên tôm he','NULL','NULL','/img/diaseases/default.png',0.00,0),(66,11,-1,'NULL','Bệnh vi bào tử trong gan tụy','NULL','NULL','/img/diaseases/default.png',0.00,0),(67,11,-1,'NULL','Bệnh sinh vật bám','NULL','NULL','/img/diaseases/default.png',0.00,0),(68,11,-1,'NULL','Bệnh giun tròn','NULL','NULL','/img/diaseases/default.png',0.00,0),(69,11,-1,'NULL','Bệnh rận tôm','NULL','NULL','/img/diaseases/default.png',0.00,0),(70,11,-1,'NULL','Bệnh thiếu Vitamin C - hội chứng chết đen','NULL','NULL','/img/diaseases/default.png',0.00,0),(71,11,-1,'NULL','Bệnh mềm vỏ ở tôm thịt','NULL','NULL','/img/diaseases/default.png',0.00,0),(72,11,-1,'NULL','Tôm bị bệnh bọt khí','NULL','NULL','/img/diaseases/default.png',0.00,0);
/*!40000 ALTER TABLE `diasease` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diaseaseAndSymptom`
--

DROP TABLE IF EXISTS `diaseaseAndSymptom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `diaseaseAndSymptom` (
  `id` int(11) NOT NULL,
  `diaseaseId` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `diaseaseId` (`diaseaseId`),
  CONSTRAINT `diaseaseAndSymptom_ibfk_1` FOREIGN KEY (`diaseaseId`) REFERENCES `diasease` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diaseaseAndSymptom`
--

LOCK TABLES `diaseaseAndSymptom` WRITE;
/*!40000 ALTER TABLE `diaseaseAndSymptom` DISABLE KEYS */;
INSERT INTO `diaseaseAndSymptom` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12),(14,14),(15,15),(16,16),(17,17),(18,18),(19,19),(20,20),(21,21),(22,22),(23,23),(24,24),(25,25),(26,26),(27,27),(28,28),(29,29),(30,30),(31,31),(32,32),(33,33),(34,34),(35,35),(36,36),(37,37),(38,38),(39,39),(40,40),(41,41),(42,42),(43,43),(44,44),(45,45),(46,46),(47,47),(48,48),(49,49),(50,50),(51,51),(52,52),(53,53),(54,54),(55,55),(56,56),(57,57),(58,58),(59,59),(60,60),(61,61),(62,62),(63,63),(64,64),(65,65),(66,66),(67,67),(68,68),(69,69),(70,70),(71,71),(72,72);
/*!40000 ALTER TABLE `diaseaseAndSymptom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diaseaseAndSymptomLine`
--

DROP TABLE IF EXISTS `diaseaseAndSymptomLine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `diaseaseAndSymptomLine` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `diaseaseAndSymptomId` int(11) DEFAULT NULL,
  `symptomValueId` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKm4m0asfuahvem70b4mycsvxfp` (`diaseaseAndSymptomId`),
  KEY `FKjalwkq7ri7vmbfr6vbq3fisxe` (`symptomValueId`),
  CONSTRAINT `FKjalwkq7ri7vmbfr6vbq3fisxe` FOREIGN KEY (`symptomValueId`) REFERENCES `symptomValue` (`id`),
  CONSTRAINT `diaseaseAndSymptom_fk_1` FOREIGN KEY (`diaseaseAndSymptomId`) REFERENCES `diaseaseAndSymptom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `symptomValue_fk_2` FOREIGN KEY (`symptomValueId`) REFERENCES `symptomValue` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1280 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diaseaseAndSymptomLine`
--

LOCK TABLES `diaseaseAndSymptomLine` WRITE;
/*!40000 ALTER TABLE `diaseaseAndSymptomLine` DISABLE KEYS */;
INSERT INTO `diaseaseAndSymptomLine` VALUES (1,1,10003),(2,1,20011),(3,1,30004),(4,1,-6),(5,1,70006),(6,1,-8),(7,1,90013),(8,1,120010),(9,1,-4),(10,1,-5),(11,1,-10),(12,1,-11),(13,1,-13),(14,1,-14),(15,1,-15),(16,1,-16),(17,1,-17),(18,2,10003),(19,2,20014),(20,2,-3),(21,2,60003),(22,2,70003),(23,2,80003),(24,2,-9),(25,2,120011),(26,2,40006),(27,2,50004),(28,2,100006),(29,2,-11),(30,2,-13),(31,2,140007),(32,2,150004),(33,2,160003),(34,2,170004),(35,18,10003),(36,18,20014),(37,18,-3),(38,18,60003),(39,18,-7),(40,18,80003),(41,18,-9),(42,18,-12),(43,18,-4),(44,18,-5),(45,18,-10),(46,18,-11),(47,18,-13),(48,18,-14),(49,18,-15),(50,18,-16),(51,18,-17),(52,3,10003),(53,3,20015),(54,3,-3),(55,3,60003),(56,3,70008),(57,3,80003),(58,3,90015),(59,3,-12),(60,3,-4),(61,3,-5),(62,3,-10),(63,3,-11),(64,3,-13),(65,3,-14),(66,3,-15),(67,3,-16),(68,3,-17),(69,27,-1),(70,27,20003),(71,27,-3),(72,27,60007),(73,27,-7),(74,27,80004),(75,27,90007),(76,27,120006),(77,27,-4),(78,27,-5),(79,27,-10),(80,27,-11),(81,27,-13),(82,27,-14),(83,27,-15),(84,27,-16),(85,27,-17),(86,4,10003),(87,4,-2),(88,4,-3),(89,4,60003),(90,4,-7),(91,4,80003),(92,4,-9),(93,4,-12),(94,4,-4),(95,4,-5),(96,4,-10),(97,4,-11),(98,4,-13),(99,4,-14),(100,4,-15),(101,4,-16),(102,4,-17),(103,6,10003),(104,6,-2),(105,6,-3),(106,6,60003),(107,6,-7),(108,6,80003),(109,6,90014),(110,6,120014),(111,6,40006),(112,6,-5),(113,6,-10),(114,6,-11),(115,6,-13),(116,6,140008),(117,6,150004),(118,6,160003),(119,6,170004),(120,31,10003),(121,31,20003),(122,31,30009),(123,31,60003),(124,31,70003),(125,31,80003),(126,31,90018),(127,31,120011),(128,31,40006),(129,31,50004),(130,31,100005),(131,31,110005),(132,31,-13),(133,31,-14),(134,31,150004),(135,31,160003),(136,31,170004),(137,17,-1),(138,17,-2),(139,17,-3),(140,17,-6),(141,17,-7),(142,17,-8),(143,17,-9),(144,17,120015),(145,17,-4),(146,17,-5),(147,17,-10),(148,17,-11),(149,17,-13),(150,17,-14),(151,17,-15),(152,17,-16),(153,17,-17),(154,8,-1),(155,8,-2),(156,8,-3),(157,8,-6),(158,8,-7),(159,8,-8),(160,8,-9),(161,8,-12),(162,8,-4),(163,8,-5),(164,8,-10),(165,8,-11),(166,8,-13),(167,8,-14),(168,8,150005),(169,8,-16),(170,8,-17),(171,9,10003),(172,9,-2),(173,9,-3),(174,9,60003),(175,9,-7),(176,9,80004),(177,9,-9),(178,9,120007),(179,9,-4),(180,9,-5),(181,9,-10),(182,9,-11),(183,9,-13),(184,9,-14),(185,9,-15),(186,9,-16),(187,9,-17),(188,29,10003),(189,29,20003),(190,29,-3),(191,29,60003),(192,29,70003),(193,29,-8),(194,29,90003),(195,29,-12),(196,29,40003),(197,29,-5),(198,29,-10),(199,29,-11),(200,29,-13),(201,29,140003),(202,29,-15),(203,29,-16),(204,29,-17),(205,32,10003),(206,32,20003),(207,32,-3),(208,32,60004),(209,32,70003),(210,32,80003),(211,32,-9),(212,32,-12),(213,32,40004),(214,32,50003),(215,32,100003),(216,32,-11),(217,32,-13),(218,32,140004),(219,32,-15),(220,32,-16),(221,32,-17),(222,12,-1),(223,12,-2),(224,12,-3),(225,12,-6),(226,12,-7),(227,12,-8),(228,12,-9),(229,12,-12),(230,12,-4),(231,12,-5),(232,12,-10),(233,12,110003),(234,12,-13),(235,12,-14),(236,12,-15),(237,12,-16),(238,12,-17),(239,24,-1),(240,24,20008),(241,24,-3),(242,24,-6),(243,24,-7),(244,24,-8),(245,24,-9),(246,24,-12),(247,24,-4),(248,24,50004),(249,24,-10),(250,24,-11),(251,24,-13),(252,24,-14),(253,24,-15),(254,24,-16),(255,24,-17),(256,25,10004),(257,25,-2),(258,25,-3),(259,25,60003),(260,25,-7),(261,25,80004),(262,25,-9),(263,25,120007),(264,25,-4),(265,25,-5),(266,25,-10),(267,25,-11),(268,25,-13),(269,25,-14),(270,25,-15),(271,25,-16),(272,25,-17),(273,19,10003),(274,19,20003),(275,19,-3),(276,19,60003),(277,19,-7),(278,19,80003),(279,19,90016),(280,19,120006),(281,19,-4),(282,19,-5),(283,19,-10),(284,19,-11),(285,19,-13),(286,19,-14),(287,19,-15),(288,19,-16),(289,19,-17),(290,20,10003),(291,20,-2),(292,20,30004),(293,20,60003),(294,20,70009),(295,20,80003),(296,20,90018),(297,20,120013),(298,20,-4),(299,20,-5),(300,20,-10),(301,20,-11),(302,20,-13),(303,20,-14),(304,20,-15),(305,20,-16),(306,20,-17),(307,28,10003),(308,28,20011),(309,28,-3),(310,28,-6),(311,28,70006),(312,28,-8),(313,28,90013),(314,28,120010),(315,28,-4),(316,28,-5),(317,28,-10),(318,28,-11),(319,28,-13),(320,28,-14),(321,28,-15),(322,28,-16),(323,28,-17),(324,10,10004),(325,10,-2),(326,10,-3),(327,10,60005),(328,10,-7),(329,10,-8),(330,10,90005),(331,10,120004),(332,10,-4),(333,10,-5),(334,10,-10),(335,10,-11),(336,10,-13),(337,10,-14),(338,10,-15),(339,10,-16),(340,10,-17),(341,33,10003),(342,33,-2),(343,33,30005),(344,33,60008),(345,33,-7),(346,33,80004),(347,33,90008),(348,33,-12),(349,33,-4),(350,33,-5),(351,33,-10),(352,33,-11),(353,33,-13),(354,33,140005),(355,33,150003),(356,33,-16),(357,33,170003),(358,34,-1),(359,34,20009),(360,34,30007),(361,34,60003),(362,34,-7),(363,34,-8),(364,34,90010),(365,34,120008),(366,34,40003),(367,34,-5),(368,34,-10),(369,34,-11),(370,34,-13),(371,34,140006),(372,34,-15),(373,34,-16),(374,34,170003),(375,26,-1),(376,26,20010),(377,26,-3),(378,26,60003),(379,26,70005),(380,26,80004),(381,26,90011),(382,26,120009),(383,26,-4),(384,26,-5),(385,26,-10),(386,26,-11),(387,26,-13),(388,26,-14),(389,26,-15),(390,26,-16),(391,26,-17),(392,22,-1),(393,22,20003),(394,22,-3),(395,22,-6),(396,22,-7),(397,22,-8),(398,22,90004),(399,22,120003),(400,22,-4),(401,22,-5),(402,22,-10),(403,22,-11),(404,22,-13),(405,22,-14),(406,22,-15),(407,22,-16),(408,22,-17),(409,11,-1),(410,11,-2),(411,11,-3),(412,11,-6),(413,11,-7),(414,11,-8),(415,11,-9),(416,11,-12),(417,11,-4),(418,11,-5),(419,11,-10),(420,11,110003),(421,11,-13),(422,11,-14),(423,11,-15),(424,11,-16),(425,11,-17),(426,30,10003),(427,30,-2),(428,30,-3),(429,30,60003),(430,30,-7),(431,30,80003),(432,30,-9),(433,30,-12),(434,30,-4),(435,30,-5),(436,30,-10),(437,30,-11),(438,30,-13),(439,30,-14),(440,30,-15),(441,30,-16),(442,30,-17),(443,7,-1),(444,7,-2),(445,7,30010),(446,7,-6),(447,7,-7),(448,7,-8),(449,7,-9),(450,7,-12),(451,7,-4),(452,7,-5),(453,7,-10),(454,7,-11),(455,7,-13),(456,7,-14),(457,7,-15),(458,7,-16),(459,7,-17),(460,16,10004),(461,16,20006),(462,16,-3),(463,16,60003),(464,16,-7),(465,16,-8),(466,16,90006),(467,16,120008),(468,16,-4),(469,16,-5),(470,16,-10),(471,16,-11),(472,16,-13),(473,16,-14),(474,16,-15),(475,16,-16),(476,16,-17),(477,14,-1),(478,14,-2),(479,14,30008),(480,14,-6),(481,14,-7),(482,14,-8),(483,14,90012),(484,14,-12),(485,14,-4),(486,14,-5),(487,14,-10),(488,14,-11),(489,14,-13),(490,14,-14),(491,14,-15),(492,14,-16),(493,14,-17),(494,15,10003),(495,15,20012),(496,15,30004),(497,15,60003),(498,15,70003),(499,15,80003),(500,15,90014),(501,15,120011),(502,15,40006),(503,15,50004),(504,15,100005),(505,15,110005),(506,15,-13),(507,15,-14),(508,15,150004),(509,15,160003),(510,15,170004),(511,21,-1),(512,21,-2),(513,21,-3),(514,21,60005),(515,21,-7),(516,21,-8),(517,21,90005),(518,21,120015),(519,21,-4),(520,21,-5),(521,21,-10),(522,21,-11),(523,21,-13),(524,21,-14),(525,21,-15),(526,21,-16),(527,21,-17),(545,5,10003),(546,5,-2),(547,5,-3),(548,5,60003),(549,5,70009),(550,5,80003),(551,5,90017),(552,5,120013),(553,5,-4),(554,5,-5),(555,5,100007),(556,5,-11),(557,5,-13),(558,5,-14),(559,5,-15),(560,5,-16),(561,5,-17),(562,23,-1),(563,23,20003),(564,23,-3),(565,23,60007),(566,23,-7),(567,23,80004),(568,23,90007),(569,23,120006),(570,23,-4),(571,23,-5),(572,23,-10),(573,23,-11),(574,23,-13),(575,23,-14),(576,23,-15),(577,23,-16),(578,23,-17),(600,35,-1),(601,35,20017),(602,35,30011),(603,35,-4),(604,35,-5),(605,35,-6),(606,35,70009),(607,35,-8),(608,35,-9),(609,35,-10),(610,35,-11),(611,35,120016),(612,35,-13),(613,35,-14),(614,35,-15),(615,35,-16),(616,35,-17),(617,36,-1),(618,36,-2),(619,36,-3),(620,36,-4),(621,36,-5),(622,36,-6),(623,36,-7),(624,36,-8),(625,36,-9),(626,36,-10),(627,36,-11),(628,36,120017),(629,36,-13),(630,36,-14),(631,36,-15),(632,36,-16),(633,36,-17),(634,37,-1),(635,37,-2),(636,37,-3),(637,37,-4),(638,37,-5),(639,37,-6),(640,37,-7),(641,37,-8),(642,37,-9),(643,37,-10),(644,37,-11),(645,37,120006),(646,37,-13),(647,37,-14),(648,37,-15),(649,37,-16),(650,37,-17),(651,38,-1),(652,38,20003),(653,38,30012),(654,38,-4),(655,38,-5),(656,38,-6),(657,38,-7),(658,38,-8),(659,38,90012),(660,38,-10),(661,38,-11),(662,38,120003),(663,38,-13),(664,38,-14),(665,38,-15),(666,38,-16),(667,38,-17),(668,39,-1),(669,39,-2),(670,39,-3),(671,39,-4),(672,39,-5),(673,39,60003),(674,39,-7),(675,39,-8),(676,39,-9),(677,39,-10),(678,39,-11),(679,39,120016),(680,39,-13),(681,39,-14),(682,39,-15),(683,39,-16),(684,39,-17),(685,40,-1),(686,40,-2),(687,40,-3),(688,40,-4),(689,40,-5),(690,40,60003),(691,40,-7),(692,40,-8),(693,40,90018),(694,40,-10),(695,40,-11),(696,40,120016),(697,40,-13),(698,40,-14),(699,40,-15),(700,40,-16),(701,40,-17),(702,41,10004),(703,41,-2),(704,41,-3),(705,41,-4),(706,41,-5),(707,41,60009),(708,41,-7),(709,41,-8),(710,41,90018),(711,41,-10),(712,41,110004),(713,41,-12),(714,41,-13),(715,41,-14),(716,41,-15),(717,41,-16),(718,41,-17),(719,42,-1),(720,42,-2),(721,42,-3),(722,42,-4),(723,42,-5),(724,42,-6),(725,42,-7),(726,42,-8),(727,42,-9),(728,42,-10),(729,42,-11),(730,42,120018),(731,42,-13),(732,42,-14),(733,42,-15),(734,42,-16),(735,42,-17),(736,43,10004),(737,43,-2),(738,43,30008),(739,43,-4),(740,43,-5),(741,43,60010),(742,43,-7),(743,43,-8),(744,43,-9),(745,43,-10),(746,43,-11),(747,43,-12),(748,43,-13),(749,43,-14),(750,43,-15),(751,43,-16),(752,43,-17),(753,44,-1),(754,44,-2),(755,44,30008),(756,44,-4),(757,44,-5),(758,44,60010),(759,44,-7),(760,44,-8),(761,44,90019),(762,44,-10),(763,44,-11),(764,44,120019),(765,44,-13),(766,44,-14),(767,44,-15),(768,44,-16),(769,44,-17),(770,45,10004),(771,45,20003),(772,45,30013),(773,45,40007),(774,45,-5),(775,45,60003),(776,45,-7),(777,45,-8),(778,45,-9),(779,45,-10),(780,45,-11),(781,45,-12),(782,45,-13),(783,45,-14),(784,45,-15),(785,45,-16),(786,45,-17),(787,46,10003),(788,46,-2),(789,46,-3),(790,46,-4),(791,46,-5),(792,46,60003),(793,46,-7),(794,46,-8),(795,46,90020),(796,46,100008),(797,46,-11),(798,46,120020),(799,46,-13),(800,46,-14),(801,46,-15),(802,46,-16),(803,46,-17),(804,47,10003),(805,47,20006),(806,47,-3),(807,47,40008),(808,47,-5),(809,47,60011),(810,47,-7),(811,47,-8),(812,47,-9),(813,47,-10),(814,47,-11),(815,47,120021),(816,47,-13),(817,47,-14),(818,47,-15),(819,47,-16),(820,47,-17),(821,48,10004),(822,48,20018),(823,48,30016),(824,48,-4),(825,48,-5),(826,48,60003),(827,48,-7),(828,48,-8),(829,48,-9),(830,48,-10),(831,48,-11),(832,48,-12),(833,48,-13),(834,48,-14),(835,48,-15),(836,48,-16),(837,48,-17),(838,49,10004),(839,49,20018),(840,49,30016),(841,49,-4),(842,49,-5),(843,49,60003),(844,49,-7),(845,49,-8),(846,49,-9),(847,49,-10),(848,49,-11),(849,49,-12),(850,49,-13),(851,49,-14),(852,49,-15),(853,49,-16),(854,49,-17),(855,50,10003),(856,50,-2),(857,50,-3),(858,50,40007),(859,50,-5),(860,50,60003),(861,50,-7),(862,50,-8),(863,50,90020),(864,50,-10),(865,50,-11),(866,50,-12),(867,50,-13),(868,50,-14),(869,50,-15),(870,50,-16),(871,50,-17),(872,51,-1),(873,51,20019),(874,51,30013),(875,51,-4),(876,51,-5),(877,51,-6),(878,51,-7),(879,51,-8),(880,51,-9),(881,51,-10),(882,51,-11),(883,51,-12),(884,51,-13),(885,51,-14),(886,51,-15),(887,51,-16),(888,51,-17),(889,52,-1),(890,52,20019),(891,52,30013),(892,52,-4),(893,52,-5),(894,52,-6),(895,52,-7),(896,52,-8),(897,52,-9),(898,52,-10),(899,52,-11),(900,52,-12),(901,52,-13),(902,52,-14),(903,52,-15),(904,52,-16),(905,52,-17),(906,53,-1),(907,53,20017),(908,53,30014),(909,53,-4),(910,53,-5),(911,53,60003),(912,53,70010),(913,53,-8),(914,53,-9),(915,53,-10),(916,53,110005),(917,53,-12),(918,53,-13),(919,53,-14),(920,53,-15),(921,53,-16),(922,53,-17),(923,54,-1),(924,54,-2),(925,54,-3),(926,54,40003),(927,54,-5),(928,54,60003),(929,54,-7),(930,54,-8),(931,54,90021),(932,54,-10),(933,54,110007),(934,54,-12),(935,54,-13),(936,54,-14),(937,54,-15),(938,54,-16),(939,54,-17),(940,55,10004),(941,55,20017),(942,55,-3),(943,55,-4),(944,55,-5),(945,55,-6),(946,55,-7),(947,55,-8),(948,55,-9),(949,55,-10),(950,55,-11),(951,55,-12),(952,55,-13),(953,55,-14),(954,55,-15),(955,55,-16),(956,55,-17),(957,56,10003),(958,56,20017),(959,56,-3),(960,56,-4),(961,56,-5),(962,56,-6),(963,56,-7),(964,56,-8),(965,56,90022),(966,56,-10),(967,56,110008),(968,56,-12),(969,56,-13),(970,56,-14),(971,56,-15),(972,56,-16),(973,56,-17),(974,57,10003),(975,57,20018),(976,57,30013),(977,57,-4),(978,57,-5),(979,57,60003),(980,57,70011),(981,57,-8),(982,57,-9),(983,57,-10),(984,57,-11),(985,57,120003),(986,57,-13),(987,57,-14),(988,57,-15),(989,57,-16),(990,57,-17),(991,58,10003),(992,58,20018),(993,58,30014),(994,58,-4),(995,58,-5),(996,58,60003),(997,58,70011),(998,58,-8),(999,58,-9),(1000,58,-10),(1001,58,-11),(1002,58,120003),(1003,58,-13),(1004,58,-14),(1005,58,-15),(1006,58,-16),(1007,58,-17),(1008,59,10003),(1009,59,20020),(1010,59,30014),(1011,59,40003),(1012,59,-5),(1013,59,60003),(1014,59,-7),(1015,59,-8),(1016,59,-9),(1017,59,-10),(1018,59,-11),(1019,59,-12),(1020,59,-13),(1021,59,-14),(1022,59,-15),(1023,59,-16),(1024,59,-17),(1025,60,10004),(1026,60,20017),(1027,60,30014),(1028,60,-4),(1029,60,-5),(1030,60,60003),(1031,60,70005),(1032,60,-8),(1033,60,-9),(1034,60,-10),(1035,60,-11),(1036,60,-12),(1037,60,-13),(1038,60,-14),(1039,60,-15),(1040,60,-16),(1041,60,-17),(1042,61,10003),(1043,61,-2),(1044,61,30015),(1045,61,-4),(1046,61,-5),(1047,61,60003),(1048,61,70011),(1049,61,-8),(1050,61,90020),(1051,61,-10),(1052,61,-11),(1053,61,-12),(1054,61,-13),(1055,61,-14),(1056,61,-15),(1057,61,-16),(1058,61,-17),(1059,62,10004),(1060,62,20021),(1061,62,-3),(1062,62,-4),(1063,62,-5),(1064,62,60003),(1065,62,-7),(1066,62,-8),(1067,62,-9),(1068,62,-10),(1069,62,-11),(1070,62,-12),(1071,62,-13),(1072,62,-14),(1073,62,-15),(1074,62,-16),(1075,62,-17),(1076,63,-1),(1077,63,20022),(1078,63,-3),(1079,63,-4),(1080,63,-5),(1081,63,-6),(1082,63,-7),(1083,63,-8),(1084,63,-9),(1085,63,-10),(1086,63,-11),(1087,63,-12),(1088,63,-13),(1089,63,-14),(1090,63,-15),(1091,63,-16),(1092,63,-17),(1093,64,10003),(1094,64,20009),(1095,64,-3),(1096,64,-4),(1097,64,-5),(1098,64,-6),(1099,64,70011),(1100,64,-8),(1101,64,-9),(1102,64,100009),(1103,64,-11),(1104,64,-12),(1105,64,-13),(1106,64,-14),(1107,64,-15),(1108,64,-16),(1109,64,-17),(1110,65,10004),(1111,65,-2),(1112,65,-3),(1113,65,40007),(1114,65,-5),(1115,65,-6),(1116,65,-7),(1117,65,-8),(1118,65,-9),(1119,65,-10),(1120,65,-11),(1121,65,-12),(1122,65,-13),(1123,65,140010),(1124,65,-15),(1125,65,-16),(1126,65,-17),(1127,66,-1),(1128,66,20017),(1129,66,-3),(1130,66,-4),(1131,66,-5),(1132,66,-6),(1133,66,-7),(1134,66,-8),(1135,66,-9),(1136,66,-10),(1137,66,-11),(1138,66,-12),(1139,66,-13),(1140,66,-14),(1141,66,-15),(1142,66,-16),(1143,66,-17),(1144,67,-1),(1145,67,20017),(1146,67,-3),(1147,67,-4),(1148,67,-5),(1149,67,-6),(1150,67,-7),(1151,67,-8),(1152,67,-9),(1153,67,-10),(1154,67,-11),(1155,67,-12),(1156,67,-13),(1157,67,-14),(1158,67,-15),(1159,67,-16),(1160,67,-17),(1161,68,10004),(1162,68,-2),(1163,68,-3),(1164,68,40010),(1165,68,-5),(1166,68,60003),(1167,68,-7),(1168,68,80003),(1169,68,-9),(1170,68,-10),(1171,68,110008),(1172,68,-12),(1173,68,-13),(1174,68,-14),(1175,68,-15),(1176,68,-16),(1177,68,-17),(1178,69,-1),(1179,69,-2),(1180,69,-3),(1181,69,-4),(1182,69,-5),(1183,69,60003),(1184,69,-7),(1185,69,-8),(1186,69,90020),(1187,69,-10),(1188,69,-11),(1189,69,-12),(1190,69,-13),(1191,69,-14),(1192,69,-15),(1193,69,-16),(1194,69,-17),(1195,70,-1),(1196,70,-2),(1197,70,-3),(1198,70,-4),(1199,70,-5),(1200,70,-6),(1201,70,-7),(1202,70,-8),(1203,70,90016),(1204,70,-10),(1205,70,-11),(1206,70,-12),(1207,70,-13),(1208,70,-14),(1209,70,-15),(1210,70,-16),(1211,70,-17),(1212,71,-1),(1213,71,-2),(1214,71,-3),(1215,71,-4),(1216,71,-5),(1217,71,-6),(1218,71,-7),(1219,71,-8),(1220,71,-9),(1221,71,100009),(1222,71,-11),(1223,71,-12),(1224,71,-13),(1225,71,-14),(1226,71,-15),(1227,71,-16),(1228,71,-17),(1229,72,10003),(1230,72,-2),(1231,72,-3),(1232,72,-4),(1233,72,-5),(1234,72,-6),(1235,72,-7),(1236,72,-8),(1237,72,-9),(1238,72,-10),(1239,72,-11),(1240,72,120022),(1241,72,-13),(1242,72,-14),(1243,72,-15),(1244,72,-16),(1245,72,-17);
/*!40000 ALTER TABLE `diaseaseAndSymptomLine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diaseaseType`
--

DROP TABLE IF EXISTS `diaseaseType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `diaseaseType` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `diaseaseTypeName` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diaseaseType`
--

LOCK TABLES `diaseaseType` WRITE;
/*!40000 ALTER TABLE `diaseaseType` DISABLE KEYS */;
INSERT INTO `diaseaseType` VALUES (-1,'Không xác định'),(1,'Bệnh ngoài da'),(2,'Bệnh ở mang'),(3,'Bệnh đường ruột'),(4,'Bệnh máu'),(5,'Bệnh ở một số cơ quan khác');
/*!40000 ALTER TABLE `diaseaseType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1),(1),(1),(1),(1),(1),(1),(1),(1),(1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likeReview`
--

DROP TABLE IF EXISTS `likeReview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `likeReview` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `reviewId` int(10) unsigned NOT NULL,
  `reviewUserId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `review_fk_1` (`reviewId`),
  KEY `user_fk_2` (`reviewUserId`),
  CONSTRAINT `review_fk_1` FOREIGN KEY (`reviewId`) REFERENCES `review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_fk_2` FOREIGN KEY (`reviewUserId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likeReview`
--

LOCK TABLES `likeReview` WRITE;
/*!40000 ALTER TABLE `likeReview` DISABLE KEYS */;
/*!40000 ALTER TABLE `likeReview` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `addLikeReview` AFTER INSERT ON `likeReview` FOR EACH ROW update review
    set likedAmount = likedAmount + 1
    where id = NEW.reviewId */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `removeLikeReview` AFTER DELETE ON `likeReview` FOR EACH ROW update review
    set likedAmount = likedAmount - 1
    where id = OLD.reviewId */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `review` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `diaseaseId` smallint(5) unsigned NOT NULL,
  `score` tinyint(3) unsigned NOT NULL,
  `comment` varchar(2000) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `likedAmount` smallint(5) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `danhGia_ibfk_1` (`userId`),
  KEY `danhGia_ibfk_2` (`diaseaseId`),
  CONSTRAINT `danhGia_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `danhGia_ibfk_2` FOREIGN KEY (`diaseaseId`) REFERENCES `diasease` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (5,4,2,5,'he','2019-05-30 14:18:36',0);
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `computeAvgScore` AFTER INSERT ON `review` FOR EACH ROW update diasease set reviewerAmount = reviewerAmount + 1, scoreAvg = (select avg(score) from review where review.diaseaseId = diasease.id) where id = NEW.diaseaseId */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `computeAvgScoreUpdate` AFTER UPDATE ON `review` FOR EACH ROW update diasease
    set scoreAvg = (select avg(score) from review where review.diaseaseId = diasease.id)
	where id = NEW.diaseaseId */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `computeAvgScoreDelete` AFTER DELETE ON `review` FOR EACH ROW update diasease
    set reviewerAmount = reviewerAmount - 1,
		scoreAvg = (select avg(score) from review where review.diaseaseId = diasease.id)
	where id = OLD.diaseaseId */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_EXPERT'),(3,'ROLE_USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seaFood`
--

DROP TABLE IF EXISTS `seaFood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `seaFood` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `seaFoodName` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seaFood`
--

LOCK TABLES `seaFood` WRITE;
/*!40000 ALTER TABLE `seaFood` DISABLE KEYS */;
INSERT INTO `seaFood` VALUES (-1,'Không xác định'),(1,'Cá trắm cỏ'),(2,'Cá rô phi'),(4,'Cá chép'),(6,'Cá tra'),(7,'Tôm sú'),(8,'Tôm chân trắng'),(9,'Tôm he'),(10,'Tôm càng xanh'),(11,'Tôm khác');
/*!40000 ALTER TABLE `seaFood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `symptom`
--

DROP TABLE IF EXISTS `symptom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `symptom` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `symptomName` varchar(200) NOT NULL,
  `symptomQuestion` varchar(500) NOT NULL,
  `symptomTypeId` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKfe5a4vtpae2jl25xd655pxn7b` (`symptomTypeId`),
  CONSTRAINT `FKfe5a4vtpae2jl25xd655pxn7b` FOREIGN KEY (`symptomTypeId`) REFERENCES `symptomType` (`id`),
  CONSTRAINT `dacDiem_ibfk_1` FOREIGN KEY (`symptomTypeId`) REFERENCES `symptomType` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `symptom`
--

LOCK TABLES `symptom` WRITE;
/*!40000 ALTER TABLE `symptom` DISABLE KEYS */;
INSERT INTO `symptom` VALUES (1,'Hoạt động bắt mồi','Hoạt động bắt mối thế nào?',1),(2,'Màu sắc da','Da có màu gì?',1),(3,'Tổn thương trên da','Trên da bị tổn thương thế nào?',1),(4,'Đặc điểm gan','Gan có đặc điểm gì',2),(5,'Đặc điểm mật','Mật có đặc điểm gì?',2),(6,'Trạng thái bơi','Hoạt động bơi thế nào?',1),(7,'Đặc điểm vây','Vây cá thế nào?',1),(8,'Vị trí cá bơi','Cá bơi ở vị trí nàoì?',1),(9,'Dấu hiệu bên ngoài khác','Có dấu hiệu khác gì không?',1),(10,'Tổn thương dưới da','Dưới da có tổn thương gì không?',1),(11,'Đặc điểm của ruột','Ruột có đặc điểm gì?',2),(12,'Đặc điểm mang','Mang có đặc điểm gì',1),(13,'Đặc điểm bóng hơi','Bóng hơi thế nào?',2),(14,'Dấu hiệu nội tạng khác','Nội tạng còn dấu hiệu gì khác?',2),(15,'Nhiệt độ','Nhiệt độ nước?',3),(16,'Độ PH','Độ PH?',3),(17,'Oxy hoà tan','Lượng Oxy hoà tan?',3);
/*!40000 ALTER TABLE `symptom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `symptomType`
--

DROP TABLE IF EXISTS `symptomType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `symptomType` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `symptomTypeName` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `symptomType`
--

LOCK TABLES `symptomType` WRITE;
/*!40000 ALTER TABLE `symptomType` DISABLE KEYS */;
INSERT INTO `symptomType` VALUES (1,'Đặc điểm bên ngoài'),(2,'Đặc điểm bên trong'),(3,'Đặc điểm môi trường');
/*!40000 ALTER TABLE `symptomType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `symptomValue`
--

DROP TABLE IF EXISTS `symptomValue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `symptomValue` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `symptomId` smallint(6) NOT NULL,
  `symptomValueDesc` varchar(500) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `indexNumber` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `symptomId_fk` (`symptomId`),
  CONSTRAINT `symptomId_fk` FOREIGN KEY (`symptomId`) REFERENCES `symptom` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=170005 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `symptomValue`
--

LOCK TABLES `symptomValue` WRITE;
/*!40000 ALTER TABLE `symptomValue` DISABLE KEYS */;
INSERT INTO `symptomValue` VALUES (-17,17,'Không rõ','NULL',1),(-16,16,'Không rõ','NULL',1),(-15,15,'Không rõ','NULL',1),(-14,14,'Không rõ','NULL',1),(-13,13,'Không rõ','NULL',1),(-12,12,'Không rõ','NULL',1),(-11,11,'Không rõ','NULL',1),(-10,10,'Không rõ','NULL',1),(-9,9,'Không rõ','NULL',1),(-8,8,'Không rõ','NULL',1),(-7,7,'Không rõ','NULL',1),(-6,6,'Không rõ','NULL',1),(-5,5,'Không rõ','NULL',1),(-4,4,'Không rõ','NULL',1),(-3,3,'Không rõ','NULL',1),(-2,2,'Không rõ','NULL',1),(-1,1,'Không rõ','NULL',1),(10003,1,'Bỏ ăn','NULL',3),(10004,1,'Giảm ăn','NULL',4),(20003,2,'Da chuyển màu xám hoặc đen','NULL',3),(20006,2,'Nhợt nhạt','NULL',6),(20008,2,'Bệnh nặng da chuyển màu vàng','NULL',8),(20009,2,'Màu nhạt','NULL',9),(20010,2,'Cơ thể cá có nhiều nhớt, màu sắc nhợt nhạt','NULL',10),(20011,2,'Viêm loét','NULL',11),(20012,2,'Xanh xám','NULL',12),(20014,2,'Có các đốm đỏ trên thân','NULL',14),(20015,2,'Trên da xuất hiện các vùng trắng xám','NULL',15),(20016,2,'Da có vết bệnh do trùng ký sinh','NULL',16),(20017,2,'Màu trắng đục','NULL',17),(20018,2,'Màu xanh, đục','NULL',18),(20019,2,'Chuyển màu hồng','NULL',19),(20020,2,'Màu sắc da biến đổi','NULL',20),(20021,2,'Màu xanh, vàng','NULL',21),(20022,2,'Màu đốm trắng','NULL',22),(30003,3,'Trung bình','NULL',3),(30004,3,'Nặng','NULL',4),(30005,3,'Xuất huyết có nhiều bạch cầu bazo','NULL',5),(30007,3,'Trong xoang bụng có chứa dịch, nội tạng bị xuất huyết, mềm nhũn','NULL',7),(30008,3,'Viêm, loét','NULL',8),(30009,3,'Cơ dưới da xuất huyết, 1 phần, toàn phần; 0 -> 1','NULL',9),(30010,3,'Trong xoang bụng và ruột có các bào nang rất lớn, làm tắc cả khúc ruột','NULL',10),(30011,3,'Trùng kí sinh','NULL',11),(30012,3,'Có nhiều dịch nhờn','NULL',12),(30013,3,'Da hoại tử','NULL',13),(30014,3,'Da, vỏ mềm','NULL',14),(30015,3,'Bị ăn mòn','NULL',15),(30016,3,'Xù xì, biến dạng','NULL',16),(40003,4,'Xuất hiện đốm trắng ở gan hoặc tỳ tạng','NULL',3),(40004,4,'Gan tái nhợt','NULL',4),(40006,4,'Gan tái nhợt màu đỏ thẫm','NULL',6),(40007,4,'Gan teo (có thể có màu trắng, vàng hoặc không)','NULL',7),(40008,4,'Gan vàng nhạt','NULL',8),(40010,4,'Hoại tử','NULL',10),(50003,5,'Mật sưng to','NULL',3),(50004,5,'Giun ký sinh trong ống mật làm tắc ống dẫn mật','NULL',4),(60003,6,'Bơi lờ đờ, yếu ớt','NULL',3),(60004,6,'Nổi lờ đờ','NULL',4),(60005,6,'Bơi lội không bình thường, quẫy mạnh','NULL',5),(60007,6,'Nhô hẳn đầu lên mặt nước và lắc mạnh; Một số cơn tách đàn, bơi quanh bờ ao','NULL',7),(60008,6,'Bơi quay tròn trong 24 h','NULL',8),(60009,6,'Bơi ngửa bụng','NULL',9),(60010,6,'Bơi cuồng loạn','NULL',10),(60011,6,'Bơi nhanh','NULL',11),(60012,6,'Mất thăng bằng','NULL',12),(70003,7,'Vây(gốc vây) xuất huyết(có thể bị rách, nát)','NULL',3),(70005,7,'Có nhiều hạt nhỏ lấm tấm màu trắng đục (đốm trắng) có thể thấy rõ bằng mắt thường','NULL',5),(70006,7,'Viêm loét','NULL',6),(70008,7,'Màu trắng nhợt, rách nát','NULL',8),(70009,7,'Có các vết trùng ký sinh','NULL',9),(70010,7,'Đỏ, hoại tử','NULL',10),(70011,7,'Bị ăn mòn hoặc gãy, đứt','NULL',11),(80003,8,'Tầng mặt','NULL',3),(80004,8,'Mặt nước','NULL',4),(90003,9,'Bụng chướng to, mắt lồi','NULL',3),(90004,9,'Da và mang có nhiều dịch nhờn','NULL',4),(90005,9,'Dị hình cong đuôi','NULL',5),(90006,9,'Da, mang có nhiều trùng bám thành các hạt lấm tấm rất nhỏ, màu hơi trắng đục, nhớt','NULL',6),(90007,9,'Thân cá có nhiều nhớt màu hơi trắng đục','NULL',7),(90008,9,'Hai mắt lồi to, bụng không chướng','NULL',8),(90009,9,'Mắt lồi đục, hậu môn viêm xuất huyết, bụng có thể chướng to','NULL',9),(90010,9,'Vây đuôi bị mòn, bụng trương to. Một số cá có mắt bị xuất huyết, mờ đục, mất một hay cả hai bên','NULL',10),(90011,9,'Lúc đầu cá tập trung gần bờ, nơi có nhiều cỏ rác, quẫy nhiều do ngứa ngáy','NULL',11),(90012,9,'Cichlidogyrus, Gyrodactylus ký sinh trên da và mang cá, làm cho mang và da cá tiết ra nhiều dịch nhờn ảnh hưởng đến hô hấp cá','NULL',12),(90013,9,'Trên da, vây, xoang miệng và mang bị tổn thương và sưng đỏ','NULL',13),(90014,9,'Mắt lồi, mờ đục','NULL',14),(90015,9,'Trứng có màu trắng đục xung quanh có sợi nấm','NULL',15),(90016,9,'Cá ngạt thở','NULL',16),(90017,9,'Chậm lớn, đầu to đuôi','NULL',17),(90018,9,'Tỷ lệ chết cao','NULL',18),(90019,9,'Viêm đỏ trên các bộ phận rận kí sinh','NULL',19),(90020,9,'Có sinh vật bám','NULL ',20),(90021,9,'Hôn mê','NULL',21),(90022,9,'Có dịch màu trắng sữa, hôi.','NULL',22),(100003,10,'Đốm đỏ','NULL',3),(100004,10,'Đốm đỏ, xuất huyết','NULL',4),(100005,10,'Đỏ tím','NULL',5),(100006,10,'Đỏ tươi','NULL',6),(100007,10,'Các vết trùng ký sinh xuất huyết, có vi khuẩn, nấm ký sinh','NULL',7),(100008,10,'Đốm trắng dưới da','NULL',8),(100009,10,'Dưới da màu đen xám','NULL',9),(110003,11,'Phá hoại tế bào thượng bì ruột cá và làm cho từng bộ phận lõm vòm','NULL',3),(110004,11,'Viêm ruột','NULL',4),(110005,11,'Ruột xuất huyết, không có thức ăn, không hoại tử','NULL',5),(110006,11,'Cơ quan nội tạng có thể xuất huyết, ruột xuất huyết có thể chứa đầy hơi, nhiều chỗ hoại tử thối nát','NULL',6),(110007,11,'Ruột màu trắng đục','NULL',7),(110008,11,'Hoại tử','NULL',8),(120003,12,'Màu đỏ không bình thường','NULL',3),(120004,12,'Bào nang bằng hạt tấm, hạt đậu xanh màu trằng đục bám trên mang','NULL',4),(120006,12,'Mang bạc trắng','NULL',6),(120007,12,'Mang có nhiều nhớt, bạc trắng','NULL',7),(120008,12,'Mang tái nhạt','NULL',8),(120009,12,'Có nhiều hạt nhỏ lấm tấm màu trắng đục (đốm trắng) có thể thấy rõ bằng mắt thường','NULL',9),(120010,12,'Sưng đỏ','NULL',10),(120011,12,'Xuất huyết, dính bùn','NULL',11),(120012,12,'Thối các sợi mang','NULL',12),(120013,12,'Mang có vết bệnh bạc trắng, hoại tử','NULL',13),(120014,12,'Mang có vết chấm lốm đốm màu đỏ và màu trắng, mang chảy máu','NULL',14),(120015,12,'Có thể nhìn thấy bào nang bằng hạt tấm, hạt đậu xanh màu trắng đục bám trên mang cá, làm kênh lắp mang không đóng lại được','NULL',15),(120016,12,'Trùng kí sinh trên mang','NULL',16),(120017,12,'Màu hồng nhạt','NULL',17),(120018,12,'Tơ mang biến dạng, ảnh hưởng hô hấp của cá\n','NULL',18),(120019,12,'Mang viêm, loét','NULL',19),(120020,12,'Mang phồng','NULL',20),(120021,12,'Mang vàng nhạt','NULL',21),(120022,12,'Mang màu đen','NULL',22),(120023,12,'Có bọt khí','NULL',23),(130003,13,'Bóng hơi đầy hơi','NULL',3),(140003,14,'Cơ quan nội tạng bị hoại tử thành đốm màu trắng đục','NULL',3),(140004,14,'Xoang bụng xuất huyết ','NULL',4),(140005,14,'Ống thận bị teo lại','NULL',5),(140006,14,'Thận trước sưng phù có màu tái nhợt','NULL',6),(140007,14,'Cơ quan nội tạng có thể xuất huyết, ruột xuất huyết có thể chứa đầy hơi, nhiều chỗ hoại tử thối nát','NULL',7),(140008,14,'Các cơ quan bám chặt vào xoang cơ thể và xuất hiện các chấm lốm đốm','NULL',8),(140009,14,'Nhân thoái hóa, kết đặc và phân mảnh','NULL',9),(140010,14,'Phân màu trắng','NULL',10),(150003,15,'Cao','NULL',3),(150004,15,'25 - 30','NULL',4),(150005,15,'18 - 29','NULL',5),(160003,16,'< 7 hoặc > 9','NULL',3),(170003,17,'Ít','NULL',3),(170004,17,'< 3 mg/l','NULL',4);
/*!40000 ALTER TABLE `symptomValue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encryptedPassword` varchar(128) NOT NULL,
  `roleId` tinyint(4) NOT NULL DEFAULT '3',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `startedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(100) DEFAULT '/img/profile/default.png',
  PRIMARY KEY (`id`),
  KEY `user_ibfk_1` (`roleId`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin@gmail.com','$2a$10$fuBNmv0RHFzl0CYF8.9q.eJIG.lzppfQMcWIHVVYK2scLtGkCfciq',1,1,'2019-04-16 08:36:42','/img/profile/default.png'),(2,'expert','expert@gmail.com','$2a$10$L.c50ujzrvRWjGGsSIZvSOSUkg9hH.Qu72K8XosngSvsN/D6mlgEO',2,1,'2019-04-16 08:37:42','/img/profile/default.png'),(3,'user','user@gmail.com','$2a$10$L.c50ujzrvRWjGGsSIZvSOSUkg9hH.Qu72K8XosngSvsN/D6mlgEO',3,1,'2019-04-16 08:38:01','/img/profile/default.png'),(4,'HIEN PHAM','user1@gmail.com','$2a$10$RlxP/Dd/gzgqKBbZsydTOuZqlUUTFZpns0Jei1BRTZqnRnqML6S.q',3,0,'2019-05-28 10:29:22','/img/profile/default.png'),(8,'Hien hai','hien2@gmail.com','$2a$10$MYNqK90qLl0ELmUrvGvYzuWmf1gpQvWLqwQp8ctWqsgYNuFhrgsxO',3,0,'2019-05-31 16:41:52','/img/profile/default.png');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-03 15:48:37
