$(document).ready(function() {
    bindingUserData();
    getSavedData();
    edittingListenner();
    
});
function edittingListenner() {
    $(".edit-image-btn").click(function(event) {
        event.preventDefault();
        $("#edit-image-modal").modal("show");
    });

    $(".change-password-btn").click(function(event) {
        event.preventDefault();
        $("#change-password-modal").modal("show");
    });

    let readURL = function(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".file-upload").on('change', function(){
        readURL(this);
    });

    $("#edit-image-modal .save-btn").click(function(event) {
        let image = $("#edit-image-modal img").attr("src");
        console.log(image);
        let imageJson = {};
        imageJson["image"] = image;
        $.ajax({
            type: "POST",
            url: "/users/updateImage",
            contentType: "application/json",
            data: JSON.stringify(imageJson) 
        }).done(function(data) {
            console.log(JSON.stringify(data));
            $(".emp-profile .profile-img img").attr("src", image);
            $("#edit-image-modal .profile-img img").attr("src", image);
            $("#navbar-id img.profile-pic").attr("src", image);
            sessionStorage.setItem("profilePic", `/img/profile/${sessionStorage.getItem("userId")}.png`);
            let successAlert = getSuccessAlert();
            $("#edit-image-modal .modal-body").append(successAlert);
            setTimeout(function() {
                $(successAlert).remove();
                $("#edit-image-modal").modal("hide");
            }, 1000);
        }).fail(function(error) {
            console.log(JSON.stringify(error));
            let dangerAlert = getDangerAlert();
            $(dangerAlert).find("div").text("Lưu không thành công, xin vui lòng thử lại!");
            $("#edit-image-modal .modal-body").append(dangerAlert);
            setTimeout(function() {
                $(dangerAlert).remove();
            }, 4000);
        });
    });

    $("#change-password-modal .save-btn").click(function(event) {
        let changedPassJson = getChangedPassInfo();
        console.log(JSON.stringify(changedPassJson));
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/users/updatePassword",
            data: JSON.stringify(changedPassJson)
        }).done(function(data) {
            console.log(JSON.stringify(data));
            let successAlert = getSuccessAlert();
            $("#change-password-modal .modal-body").append(successAlert);
            setTimeout(function() {
                $(successAlert).remove();
                window.location.reload();
            }, 1000);
        }).fail(function(error) {
            console.log(JSON.stringify(error));
            let dangerAlert = getDangerAlert();
            $("#change-password-modal .modal-body").append(dangerAlert);
            setTimeout(function() {
                $(dangerAlert).remove();
            }, 4000);
        });
    })
}

function getDangerAlert() {
    return $("#danger-alert").clone().removeClass("hide");
}

function getSuccessAlert() {
    return $("#success-alert").clone().removeClass("hide");
}

function getChangedPassInfo() {
    let result = {}
    let currentPassword = $("#change-password-modal .cur-pass input").val();
    let newPassword = $("#change-password-modal .new-pass input").val();
    result["currentPassword"] = currentPassword;
    result["newPassword"] = newPassword;
    return result;
}

function setDeleteListen() {
    $(".delete").click(function(event) {
        event.preventDefault();
        let saveId = $(this).attr("saveId");
        console.log(saveId);
        setDeleteAction(saveId);
    });
}
function setDeleteAction(saveId) {
    $.ajax({
        type: "DELETE",
        url: "/secure_api/diagnosis/save/" + saveId
    }).done(data => {
        console.log("delete ok: " + data);
        window.location.reload();
    }).fail(error => {
        console.log("delete error: " + error);
    });
}
function getSavedData() {
    $.ajax({
        type: "GET",
        url: "/secure_api/diagnosis/saved"
    }).done(data => {
        console.log("ok: " + data);
        data = JSON.parse(data);
        bindingSavedData(data);
        setDeleteListen();
    }).fail(error => {
        console.log("error: " + error);
    });
}

function bindingSavedData(data) {
    $(".emp-profile #diagnosisedNum").text(data.length);
    for (let i = 0; i < data.length; i++) {
        let savedEle = getSaveEle();
        setDataForSaveEle(data[i], savedEle);
        $("#savedList tbody").append(savedEle);
    }
}

function bindingUserData() {
    $(".emp-profile #name").text(sessionStorage.getItem("userName"));
    $(".emp-profile #email").text(sessionStorage.getItem("email"));
    $(".emp-profile #startedAt").text(getDateFromStr(sessionStorage.getItem("startedAt")));
    $(".emp-profile .profile-img img").attr("src", sessionStorage.getItem("profilePic"));
    $("#edit-image-modal .profile-img img").attr("src", sessionStorage.getItem("profilePic"));
}

function setDataForSaveEle(data, ele) {
    $(ele).attr("saveId", data.id);
    $(ele).find("a.delete").attr("saveId", data.id);
    $(ele).find(".date").text(getDateFromStr(data.savedTime));
    let symptomStr = data.symptoms;
    let array = symptomStr.split(";");
    for (let i = 0; i < array.length; i++) {
        let symptomEle = getSymptomEle();
        $(symptomEle).text(array[i]);
        $(ele).find(".symptoms").append(symptomEle);
    }
    let diaseaseStr = data.diaseases;
    let diaseaseArr = diaseaseStr.split(";");
    for (let i = 0; i < diaseaseArr.length - 1; i++) {
        let subArr = diaseaseArr[i].split(":");
        console.log(subArr[1] + ": " + subArr[2] + "%");
        let diaseaseEle = getDiaseaseEle();
        $(diaseaseEle).text(subArr[1] + ": " + subArr[2] + "%");
        $(ele).find(".diaseases").append(diaseaseEle);
    }

}

function getDiaseaseEle() {
    return $("tbody .saved-ele.hide .diasease-ele.hide").clone().removeClass("hide");
}

function getSymptomEle() {
    return $("tbody .saved-ele.hide .symptom-ele.hide").clone().removeClass("hide");
}
function getSaveEle() {
    return $("#savedList .saved-ele.hide").clone().removeClass("hide");
}

function getDateFromStr(str) {
    let date = new Date(str);
    let result =  date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();
    console.log("date: " + result); 
    return result;
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/; domain=.benhthuysan.vn";
}

function deleteCookie(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/; domain=.benhthuysan.vn";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
        c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
        }
    }
    return "";
}
