const OUT_SYMPTOM_ID = 1;
const IN_SYMPTOM_ID = 2;
const ENV_SYMPTOM_ID = 3;
const EMPTY_SELECTED_SUGGESTION = "Bạn đã không chọn các triệu chứng được trang web gợi ý. Có thể kết quả chẩn đoán sẽ không chính xác!" + "\n"
+ "Bạn vẫn muốn tiếp tục sử dụng những triệu chứng đã nhập để chẩn đoán?";
const STOP_SUGGESTION = "Đã tìm thấy bệnh! Bạn có muốn chẩn đoán ngay bây giờ hoặc tiếp tục chọn các triệu chứng khác?";
const EMPTY_SELECTED_SYMPTOM = "Bạn chưa chọn mô tả triệu chứng. Hãy chọn các triệu chứng và thử lại!";
const EMPTY_SEAFOOD = "Hãy chọn loài thủy sản trước khi chẩn đoán!";

const numAndClassStar = {
	"0": "far fa-star star-mark",
	"0.5": "fas fa-star-half-alt star-mark",
	"1": "fas fa-star star-mark"
};

let diaseaseResult, suggestStr, selectedSuggestSymptoms, nextIndex;

$(document).ready(function () {
	bindingSeafoodsData();
	$("#no-result").show();
	// $("#result-content").css("display", "none");
	$("#result-content").hide();
	$("#diagnosis-btn").on("click", function () {
		doFilterAndDiagnosis();
	});
});

function doFilterAndDiagnosis() {
	if (isNotSelectSeaFood()) {
		$("#alert-modal .modal-body .message").text(EMPTY_SEAFOOD);
		$("#alert-modal").modal('show');
	} else if (isEmptySelectedSymptoms()) {
		$("#alert-modal .modal-body .message").text(EMPTY_SELECTED_SYMPTOM);
		$("#alert-modal").modal('show');
	} else if (isNotUsedSuggestion()) {
		let alert = $("#confirm-modal").clone().attr("id", "confirm-modal-clone");
		$(alert).find(".modal-body .message").text(EMPTY_SELECTED_SUGGESTION);
		$(alert).find(".close-btn").text("Quay lại");
		$(alert).find(".continue").text("Tiếp tục chẩn đoán");
		$(alert).find(".continue").click(function() {
			$(alert).modal('hide');
			$(alert)
			goDiagnosis();
		})
		$(alert).modal('show');
	} else {
		goDiagnosis();
	}
	
}

function isEmptySelectedSymptoms() {
	let isEmpty = true;
	$("select.symptom-select").each(function() {
		let selectEle = $(this);
		let selectedValue = Number($(selectEle).find("option:selected").attr("value"));
		if (selectedValue > 0) {
			isEmpty = false;
		}
	});
	return isEmpty;
}
function isNotSelectSeaFood() {
	let selectedValue = Number($("#seafood-select option:selected").attr("value"));
	if (selectedValue < 0) return true;
	return false;
}
function isNotUsedSuggestion() {
	if (selectedSuggestSymptoms.length == 0) return true;
	return false;
}

function goDiagnosis() {
	let jsonSend = getSymptomInputs();
	$.ajax({
		type: "POST",
		url: "/api/getTopResultList",
		contentType: "application/json",
		data: JSON.stringify(jsonSend)
	}).done(function (data) {
		logger("done: " + data);
		let resultData = JSON.parse(data);
		diaseaseResult = resultData;
		bindingResultData(resultData);
		window.location.href = "/diaseases/diagnosis#go-result";
		// $("#resultModal").modal('show');
	}).fail(function (error) {
		logger("error: " + error);
	});
}

function restartSuggestion(data) {
	$(".suggesting-symptoms[id!='seafood-select']").removeClass("suggesting-symptoms");
	$(".suggested-symptoms[id!='seafood-select']").removeClass("suggested-symptoms");
	console.log("restarting suggestion...");
	suggestStr = data;
	selectedSuggestSymptoms = [];
	nextIndex = 0;
	console.log("starting loop");
	while (1) {
		let nextSymptomId = getSymptomIdFromIndex(suggestStr, nextIndex);
		console.log("nextSymptomId: " + nextSymptomId);
		let value = getSelectedValue(nextSymptomId);
		console.log("get in DOM value: " + value);
		if (value < 0) {
			console.log("breaking...");
			break;
		} else {
			selectedSuggestSymptoms.push(nextSymptomId);
			console.log("push selected: " + nextSymptomId);
			let selectedValueIndex = getIndexOfSelected(suggestStr,
				nextIndex, nextSymptomId, value);
			if (isStopSuggestion(suggestStr, selectedValueIndex)) {
				console.log("suggestion stoped.");
				break;
			} else {
				nextIndex = getNextIndex(suggestStr, selectedValueIndex);
				console.log("Next index: " + nextIndex);
			}

		}
	}
	console.log("breaked!")
	highlightSuggestSymptom();
}

function suggestDiagnosisNow() {
	$("#confirm-modal .modal-body .message").text(STOP_SUGGESTION);
	$("#confirm-modal .close-btn").text("Chọn thêm triệu chứng");
	$("#confirm-modal .continue").text("Chẩn đoán ngay");
	$("#confirm-modal .continue").click(function() {
		$("#confirm-modal").modal('hide');
		goDiagnosis();

	});
	$("#confirm-modal").modal("show");
}

function isStopSuggestion(str, index) {
	let subStr = str.substring(index);
	let ismeetValue = subStr.search(/\d+\s=\s-?\d+:/);
	if (ismeetValue == 0) {
		return true;
	} else {
		return false;
	}
}

function getSuggestingStr(seaFoodId) {
	$.ajax({
		type: "GET",
		url: "/api/diagnosis/suggestion/" + seaFoodId
	}).done(data => {
		console.log("ok: " + data);
		suggestStr = data;
		restartSuggestion(data);
		setSuggestListen();
	}).fail(error => {
		console.log("error: " + error);
	});
}



function setSuggestListen() {
	$("select.symptom-select").change(function () {
		let symptomId = Number($(this).attr("symptomId"));
		//handle with changes in selected
		if (selectedSuggestSymptoms.includes(symptomId) == true) {
			restartSuggestion(suggestStr);
		}
		//handle with changes in next suggest
		if (symptomId == getSymptomIdFromIndex(suggestStr, nextIndex)) {
			restartSuggestion(suggestStr);
		}
	});
}

function getSelectedValue(symptomId) {
	return $(`select[symptomId='${symptomId}']`).find("option:selected").attr("value");
}

function bindingResultData(resultData) {
	// $("#no-result").css("display", "none");
	// logger("length: " + resultData.length);
	$("#result-container").show();
	$("#no-result").hide();
	$("#result-list").empty();
	for (let i = 0; i < resultData.length; i++) {
		if (resultData[i].percentage > 0) {
			let resultEle = getCloneDiaseaseEle();
			bindingResultDataToEle(resultData[i], resultEle);
			$("#result-list").append(resultEle);
		}
	}
	$("#result-list").append(getCloneSaveBtn());
	$("#result-list").append(getCloneAlert());
	$("#result-list .save-btn").click(function () {
		setSaveAction();
	});

}

function getCloneAlert() {
	return $("#result-list-tmp .alert-ele").clone();
}

function getCloneDiaseaseEle() {
	return $("#result-list-tmp .diasease-result").clone();
}

function getCloneSaveBtn() {
	return $("#result-list-tmp .save-ele").clone();
}

function bindingResultDataToEle(data, ele) {
	let diaseaseData = data.diaseaseData;
	let percentage = Number((100 * data.percentage).toFixed(2));
	logger("per: " + percentage);
	$(ele).attr("diaseaseId", diaseaseData.id);
	$(ele).find(".percentage").text(percentage);
	$(ele).find(".diasease-name").text(diaseaseData.diaseaseName);
	$(ele).find(".review-amount").text(diaseaseData.reviewerAmount);
	$(ele).find(".review-score").text(diaseaseData.scoreAvg);
	$(ele).find(".seaFood-name").text(diaseaseData.seaFood.seaFoodName);
	$(ele).find(".diasease-type").text(diaseaseData.diaseaseTypeName);
	$(ele).find(".treatment").text(diaseaseData.treatment.substring(0, 150));
	$(ele).find(".diasease-href").attr("href", "/diaseases/" + diaseaseData.id);
	$(ele).find(".image-area").attr("src", diaseaseData.image);
	setStarRatingEle(diaseaseData.scoreAvg, $(ele).find(".comment-star"));
}

function bindingSeafoodsData() {

	$.ajax({
		type: "GET",
		url: "/api/seaFoods"
	}).done(function (data) {
		// console.log(data);
		data.filter(e => e.id != -1).forEach(e => {
			let optionEle = $("#seafood-select option:first").clone();
			$(optionEle).attr("selected", false);
			$(optionEle).attr("value", e.id);
			$(optionEle).text(e.seaFoodName);
			$("#seafood-select").append($(optionEle));
		});
		let seaFoodEle = $("#seafood-select");
		$(seaFoodEle).addClass("suggesting-symptoms");
		$(seaFoodEle).change(function() {
			$(seaFoodEle).removeClass("suggesting-symptoms");
			$(seaFoodEle).addClass("suggested-symptoms");
			$("#seafood-select option[value='-1']").remove();
			bindingSymptomsData();
			$("#diagnosis-btn").show();
		});
		
	}).fail(function (error) {
		console.log(error);
	});
}

function bindingSymptomsData() {
	// TODO: remove all created symptom 
	$(".symptom-form.new").remove();
	$("#symptom-inputs").removeClass("hide");
	let selectedSeaFoodId = getSelectedSeaFoodId();
	if (selectedSeaFoodId < 0) {
		$("#seafood-select").addClass("suggesting-symptoms");
	} else {
		getSymptomsData(selectedSeaFoodId);
	}
}

function getSymptomsData(seaFoodId) {
	$.ajax({
		type: "GET",
		url: "/api/symptomTypesAndInner?seaFoodId=" + seaFoodId
	}).done(function (data) {
		data = JSON.parse(data);
		// console.log("debug data: " + data);
		for (let i = 0; i < data.length; i++) {
			// console.log("debug data[i]: " + data[i]);
			// get tmp ele and remove it latter
			let symptomTypeEle = getSymptomTypeEle(data[i].id);

			// forEach symptom
			data[i].symptoms.forEach(symptom => {
				//set symptom id
				let symptomEle = getSymptomEle();
				$(symptomEle).find("a[name]").attr("name", "suggesting-" + symptom.id);
				$(symptomEle).attr("datasymptom", symptom.id);
				$(symptomEle).find("select").attr("symptomId", symptom.id);
				// set lable
				$(symptomEle).find("label:first").text(symptom.symptomName);
				// forEach symptomValue
				if (symptom.symptomValues.some(e => Number(e.id) < 0) == false) {
					let unknowOption = getOptionEle();
					$(unknowOption).attr("value", -symptom.id);
					$(unknowOption).attr("selected", true);
					$(unknowOption).text("Không rõ");
					$(symptomEle).find("select").append(unknowOption);
				}
				symptom.symptomValues.sort((a, b) => a.indexNumber - b.indexNumber)
					.forEach(value => {
						// set option value
						let optionClone = getOptionEle();
						$(optionClone).attr("value", value.id);
						$(optionClone).attr("selected", (value.indexNumber === 1) ? true : false);
						$(optionClone).text(value.symptomValueDesc);
						$(symptomEle).find("select").append(optionClone);
					});
				$(symptomTypeEle).append(symptomEle);
			});
		}
		getSuggestingStr(seaFoodId);
	}).fail(function (error) {
		console.log("error: " + JSON.stringify(error));
	});
}
function getSymptomEle() {
	let ele =  $(".symptom-form.hide.sample").clone()
				.removeClass("hide sample").addClass("new");
	$(ele).find("option").remove();
	return ele;
}

function getOptionEle() {
	return $(".symptom-form.sample option:first").clone().removeClass("hide");
}

function getSelectedSeaFoodId() {
	return $("#seafood-select option:selected").attr("value");
}

function highlightSuggestSymptom() {
	console.log("highlighting...");
	for (let i = 0; i < selectedSuggestSymptoms.length; i++) {
		console.log("highlight for selected: " + selectedSuggestSymptoms[i]);
		let selectEle = $(`div[datasymptom='${selectedSuggestSymptoms[i]}']`).find("select:first");
		removeHighlight(selectEle);
		$(selectEle).addClass("suggested-symptoms");
		
	}
	let suggestingSymptom = getSymptomIdFromIndex(suggestStr, nextIndex);
	console.log("highlight for suggesting: " + suggestingSymptom);
	let selectEle = $(`select[symptomId='${suggestingSymptom}']`);
	removeHighlight(selectEle);
	if (getSelectedValue(suggestingSymptom) < 0) {
		$(selectEle).addClass("suggesting-symptoms");
		setTimeout(function() {
			window.location.href = "/diaseases/diagnosis#suggesting-" + suggestingSymptom;
		}, 100);
		
	} else {
		$(selectEle).addClass("suggested-symptoms");
	}
	
}
function removeHighlight(ele) {
	$(ele).removeClass("suggested-symptoms");
	$(ele).removeClass("suggesting-symptoms");
}

function getSymptomInputs() {
	let jsonSend = {};
	let seafood_id = $("#seafood-select option:selected").val();
	jsonSend['seafood_id'] = seafood_id;
	let symptoms = [];
	$(".symptom-form[datasymptom!='0']").each(function () {
		let symptomObj = {};
		symptomObj['symptom_id'] = $(this).attr("datasymptom");
		console.log("symptom_id=" + $(this).attr("datasymptom"));
		symptomObj['symptom_value'] = $(this).find("option:selected").val();
		console.log("value=" + $(this).find("option:selected").val());
		symptoms.push(symptomObj);
	});
	jsonSend['symptoms'] = symptoms;
	console.log("inputed: " + JSON.stringify(jsonSend));
	return jsonSend;
}

function getSymptomTypeEle(id) {
	return $(`[symptomType='${id}']`);
}

function appendEleInto(ele, id) {
	if (id === OUT_SYMPTOM_ID) {
		$("#out-symptom-gr").append(ele);
	} else if (id === IN_SYMPTOM_ID) {
		// binding in-symptom
		return $("#in-symptom-gr").append(ele);
	} else {
		// binding env-symptom
		return $("#env-symptom-gr").append(ele);
	}
}

function logger(str) {
	console.log("mylog: " + str);
}

function setStarRatingEle(rating_num, ele) {
	$(ele).find("i").each(function () {
		let num = getStarNum(this);
		setStarClass(computeClassCode(num, rating_num), this);
	});
}

function getStarNum(ele) {
	return parseInt($(ele).attr("starnum"));
}

// 1-> "fas far-star", 0.5 -> "fas fa-star-half-alt", 0 -> "far far-star"
function setStarClass(classCode, ele) {
	$(ele).attr("class", numAndClassStar[classCode]);
}

function computeClassCode(num, rating_num) {
	if (num <= rating_num) {
		return "1";
	} else if (num < rating_num + 1) {
		return "0.5";
	} else {
		return "0";
	}
}

function setSaveAction() {
	let sendData = getSaveDataFromDOM();
	console.log("SEND DATA: " + JSON.stringify(sendData))
	$.ajax({
		type: "POST",
		url: "/secure_api/diagnosis/save",
		contentType: "application/json",
		data: JSON.stringify(sendData)
	}).done(data => {
		console.log("done: " + data)
		let alertEle = $("#result-list .alert-ele").removeClass("hide")
		$(alertEle).slideUp(0);
		$(alertEle).slideDown('fast', function () {
			$(alertEle).delay(3000).slideUp('slow', function () {
				// $(alertEle).addClass("hide");
			});
		});
	}).fail(error => {
		console.log("error:" + error)
	});
}

function getSaveDataFromDOM() {
	let sendData = {};
	let seaFoodId = $("#seafood-select option:selected").val();
	let symptomsStr = getSymptomsStrFromDOM();
	let diaseasesStr = getDiaseasesStrFromDOM();
	sendData["seaFoodId"] = seaFoodId;
	sendData["symptoms"] = symptomsStr;
	sendData["diaseases"] = diaseasesStr;
	return sendData;
}

function getSymptomsStrFromDOM() {
	let symptoms = "";
	$(".symptom-form[datasymptom!='0']").each(function () {
		let symptomName = $(this).find("label:first").text();
		let symptomValue = $(this).find("option:selected").text();
		if (symptomValue != "Không rõ") {
			symptoms = symptoms.concat(symptomName + ":" + symptomValue + ";");
		}
	});
	return symptoms;
}

function getDiaseasesStrFromDOM() {
	let diaseases = "";
	$("#result-list .diasease-result").each(function () {
		let diaseaseId = $(this).attr("diaseaseId");
		let diaseaseName = $(this).find(".diasease-name").text();
		let percentage = $(this).find(".percentage").text();
		diaseases = diaseases.concat(diaseaseId + ":" + diaseaseName + ":" + percentage + ";");
	});
	return diaseases;
}

function getSymptomIdFromIndex(str, index) {
	let subStr = str.substring(index);
	let idStr = subStr.split("=", 1)[0];
	return Number(idStr);
}

// function getValueFromIndex(str, index) {
// 	let subStr = str.substring(index);
// 	let value = Number(subStr.split(/[=,|]/)[1]);
// 	return value;
// }

// return -1 if not found
function getIndexOfSelected(str, currentIndex, symptomId, valueId) {
	let searchStr = symptomId + " = " + valueId;
	return str.indexOf(searchStr, currentIndex);
}

function getNextIndex(str, nextIndex) {
	let subStr = str.substring(nextIndex);
	return nextIndex + 3 + subStr.substring(3).search(/\d+\s=/);
}