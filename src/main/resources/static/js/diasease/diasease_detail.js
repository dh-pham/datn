const starAndText = {
		"0": "",
		"1": "Quá tệ!",
		"2": "Không đúng!",
		"3": "Khá đúng!",
		"4": "Chính xác!",
		"5": "Rất chính xác!",
	},
	numAndClassStar = {
		"0": "far fa-star star-mark",
		"0.5": "fas fa-star-half-alt star-mark",
		"1": "fas fa-star star-mark"
	};

let diaseaseId, diaseaseData, popularSymptomsData, reviewData, starSelecting;
let userId = sessionStorage.getItem("userId"),
	yourReviewData;
$(document).ready(function () {
	diaseaseId = getDiaseaseIdFromUrl();
	console.log("id: " + diaseaseId);
	getDiaseaseData(diaseaseId);
	getPopularSymptoms(diaseaseId);
	getReviewData(diaseaseId);
	setReviewModalListenner();
});

function getDiaseaseData(diaseaseId) {
	let urlTarget = generateDiaseaseApiUrl(diaseaseId);
	// diasease info
	$.ajax({
		method: "GET",
		url: urlTarget
	}).done(function (data) {
		// console.log("diasease: " + JSON.stringify(data));
		bindingDiaseaeView(data);
		diaseaseData = data;
	}).fail(function (error) {
		console.log("fail: " + error);
		return null;
	});
}

function getReviewData(diaseaseId) {
	// review, comment info
	$.ajax({
		method: "GET",
		url: generateReviewsApiUrl(diaseaseId)
	}).done(function (data) {
		reviewData = data;
		yourReviewData = data.content.find(function (element) {
			return element.appUser.id == userId;
		});
		bindingReviewData(data.content);
		getYourLikes();
	}).fail(function (error) {
		console.log("fail:");
	});
}

function getPopularSymptoms(diaseaseId) {
	let targetUrl = generatePopularSymptomsApiUrl(diaseaseId);
	$.ajax({
		method: "GET",
		url: targetUrl
	}).done(function (data) {
		bindingPopularSymptoms(data);
		popularSymptomsData = data;
	}).fail(function (error) {
		console.log("fail: " + JSON.stringify(error));
		return null;
	});
}

function getYourLikes() {
	$.ajax({
		type: "GET",
		url: `/api/likeReviews/diaseases/${diaseaseId}/users/${userId}`
	}).done(data => {
		// TODO: binding your like
		bindingYourLikes(data);
	}).fail(error => {
		console.log("error: " + JSON.stringify(error));
	});
}

function bindingYourLikes(data) {
	for (let i = 0; i < data.length; i++) {
		let reviewId = data[i].review.id;
		$(`.like-act a[reviewId='${reviewId}'] i`).addClass("liked");
		$(`.like-act a[reviewId='${reviewId}']`).attr("likeId", data[i].id);
	}
	setLikeListenner();
}

function bindingPopularSymptoms(data) {
	for (let i = 0; i < data.length; i++) {
		if (data[i].id > 0) {
			let symptomEle = createSymptomEle(data[i]);
			$("#symptoms-id .symptom-list").append(symptomEle);
		}
	}
}

function setReviewModalListenner() {
	starHoverListenner();
	starClickListenner();
	$("#reviewModal").on("shown.bs.modal", function (e) {
		// TODO: verify login?
		console.log("modal shown!");
		if (yourReviewData != null) {
			starSelecting = yourReviewData.score;
			setModalData(yourReviewData.score, yourReviewData.comment);
		} else {
			setModalData(0, "");
		}
		$("#reviewModal .modal-body #message-text").focus();
	});

	$("#reviewModal").on("hidden.bs.modal", function (e) {
		console.log("modal hide!");
		if (yourReviewData != null) {
			setModalData(yourReviewData.score, yourReviewData.comment);

		} else {
			starSelecting = null;
			setModalData(0, "");
		}
	});

	$("#reviewModal .modal-footer .save-btn").on("click", function (e) {
		// TODO: verify emtpy field?
		if ($("#reviewModal .score-text").text() == "" || $("#reviewModal #message-text").val() == "") {
			// alert("Hãy viết đánh giá và bình chọn số sao trước khi lưu!");
			let alertEle = $(".review-alert").clone().removeClass("hide");
			$(alertEle).slideUp(0);
			$("#reviewModal .modal-body > .comment").prepend(alertEle);
			$(alertEle).slideDown('fast', function () {
				$(alertEle).delay(3000).slideUp('slow', function () {
					$(alertEle).remove();
				});
			});
		} else {
			console.log("modal saved!");
			let score = starSelecting;
			let comment = $("#reviewModal .modal-body #message-text").val();
			let sendData = {};

			if (yourReviewData == null) {
				sendData["diaseaseId"] = diaseaseId;
				sendData["score"] = score;
				sendData["comment"] = comment;
				let targetUrl = "/api/reviews/";
				$.ajax({
					type: "POST",
					url: targetUrl,
					contentType: "application/json",
					data: JSON.stringify(sendData)
				}).done(function (data) {
					console.log("post done: " + JSON.stringify(data));
					let alertEle = $(".custom-alert").clone().removeClass("hide");
					$(alertEle).find(".alert-message").text("Đánh giá lưu thành công!");
					$("#reviewModal .modal-body > .comment").prepend(alertEle);
					setTimeout(function () {
						$(alertEle).remove();
						window.location.reload();
					}, 1000);
				}).fail(function (error) {
					console.log("post error: " + JSON.stringify(error));
					if (error.status == 401) {
						window.location.href = "/signin";
					}
				});
			} else {
				sendData["score"] = score;
				sendData["comment"] = comment;
				let targetUrl = `/api/reviews/${yourReviewData.id}`;
				$.ajax({
					type: "PUT",
					url: targetUrl,
					contentType: "application/json",
					data: JSON.stringify(sendData)
				}).done(function (data) {
					console.log("put done: " + JSON.stringify(data));
					window.location.reload();
				}).fail(function (error) {
					console.log("put error: " + JSON.stringify(error));
					if (error.status == 401) {
						window.location.href = "/signin";
					}
				});

			}
		}
	});
}

function setModalData(score, comment) {
	setStarRatingEle(score, $("#reviewModal .modal-body .five-star-voting"));
	setScoreText(starAndText[score]);
	$("#reviewModal .modal-body #message-text").val(comment);
	$("#reviewModal .modal-body .score-text").text(starAndText[score]);
}

function starClickListenner() {
	$("#reviewModal .modal-body .star-mark").on("click", function (e) {
		let num = getStarNum(this);
		starSelecting = num;
		setStarRatingEle(starSelecting, $("#reviewModal .modal-body .five-star-voting"));
	});
}

function setStarRatingEle(rating_num, ele) {
	$(ele).find("i").each(function () {
		let num = getStarNum(this);
		setStarClass(computeClassCode(num, rating_num), this);
	});
}


// 1-> "fas far-star", 0.5 -> "fas fa-star-half-alt", 0 -> "far far-star"
function setStarClass(classCode, ele) {
	$(ele).attr("class", numAndClassStar[classCode]);
}

function computeClassCode(num, rating_num) {
	if (num <= rating_num) {
		return "1";
	} else if (num < rating_num + 1) {
		return "0.5";
	} else {
		return "0";
	}
}

function bindingDiaseaeView(data) {
	$("#diasease-img-id img").attr("src", data.image);
	$("#header-info-id .diasease-name").text(data.diaseaseName);
	$("#header-info-id .seaFood-name").text(data.seaFood.seaFoodName);
	$("#header-info-id .diasease-type").text(data.diaseaseType.diaseaseTypeName);
	setStarRatingEle(data.scoreAvg, $(".diasease-star-rating"));
	$("#header-info-id .review-amount").text(data.reviewerAmount);
	$("#reviews-id .review-amount").text(data.reviewerAmount);
	$("#reviews-id .score-avg").text(data.scoreAvg == null ? 0 : data.scoreAvg);
	$("#treatment-id .treatment-text").text(data.treatment);
}


function bindingReviewData(content) {
	let commentList = $("#comment-list");
	if (content.length < 1) {
		let empty_commentEle = $("#comment-list #empty-comment").clone().removeClass("hide");
		$("#comment-list").append(empty_commentEle);
	}
	for (let i = 0; i < content.length; i++) {
		if (content[i].appUser.id == userId) {
			setHideWriteRating(true);
			setYourCommentEle(content[i]);
			if (content.length == 1) {
				let empty_commentEle = $("#comment-list #empty-comment").clone().removeClass("hide");
				$(empty_commentEle).find(".empty-text p").text("Chưa có bình luận nào khác!");
				$("#comment-list").append(empty_commentEle);
			}
		} else {
			let commentEle = cloneAndSetCommentEle(content[i]);
			$(commentList).append(commentEle);
		}
	}
}

function setHideWriteRating(isRated) {
	if (isRated == true) {
		$("#reviews-id .write-btn").addClass("hide");
	}

}

function setYourCommentEle(content) {
	let yourCommentEle = $("#reviews-id #your-review");
	$(yourCommentEle).removeClass("hide");
	$(yourCommentEle).find(".user-pic img.profile-pic").attr("src", content.appUser.image);
	$(yourCommentEle).find(".user-name").text(content.appUser.name);
	setStarRatingEle(content.score, $(yourCommentEle).find(".comment-star"));
	$(yourCommentEle).find(".publish-time").text(getDateFromStr(content.time));
	$(yourCommentEle).find(".comment-content").text(content.comment);
	// TODO: set listenner for edit and remove
	$(yourCommentEle).find(".edit-act a").on("click", function (e) {
		e.preventDefault();
		$("#reviewModal").modal('show');
	});

	$(yourCommentEle).find(".delete-act a").on("click", function (e) {
		e.preventDefault();
		$("#deleteModal").modal('show');
		$("#deleteModal .save-btn").on("click", function (e) {
			$.ajax({
				type: "DELETE",
				url: `/api/reviews/${yourReviewData.id}`
			}).done(function (data) {
				console.log("delete review ok: " + JSON.stringify(data));
				window.location.reload();
			}).fail(function (error) {
				console.log("delete review fail: " + JSON.stringify(error));
			});
		});
	});
}

function cloneAndSetCommentEle(content) {
	let commentEle = getCommentClone();
	$(commentEle).attr("id", "comment-id" + content.id);
	$(commentEle).attr("reviewId", content.id);
	$(commentEle).find(".user-pic img.profile-pic").attr("src", content.appUser.image);
	$(commentEle).find(".user-name").text(content.appUser.name);
	let starEles = $(commentEle).find(".comment-star");
	setStarRatingEle(content.score, starEles);
	$(commentEle).find(".publish-time").text(getDateFromStr(content.time));
	$(commentEle).find(".liked-amount").text(content.likedAmount);
	$(commentEle).find(".comment-content").text(content.comment);
	$(commentEle).find(".like-act a").attr("reviewId", content.id);
	return commentEle;
}

function setLikeListenner() {
	// add like-action
	$(".like-act a").on("click", function (e) {
		e.preventDefault();
		let reviewId = parseInt($(this).attr("reviewId"));
		let reviewEle = $(`#comment-list .comment-ele[reviewId='${reviewId}'`);
		if ($(reviewEle).find(".like-act i").hasClass("liked")) {
			let likeId = $(reviewEle).find(".like-act a").attr("likeId");
			// delete request
			$.ajax({
				type: "DELETE",
				url: `/api/likeReviews/${likeId}`
			}).done(data => {
				$(reviewEle).find(".like-act i").removeClass("liked");
				$(reviewEle).find(".like-act a").removeAttr("likeId");
				let likedAmount = parseInt($(reviewEle).find(".liked-amount").text());
				$(reviewEle).find(".liked-amount").text(likedAmount - 1);
				console.log("oke delete like: " + JSON.stringify(data));
			}).fail(error => {
				console.log("error: " + JSON.stringify(error));
				if (error.status == 401) {
					window.location.href = "/signin";
				}
			});
		} else {
			// post request
			let sendData = {};
			sendData["reviewId"] = reviewId;
			sendData["userId"] = userId;
			$.ajax({
				type: "POST",
				url: `/api/likeReviews/`,
				contentType: "application/json",
				data: JSON.stringify(sendData)
			}).done(result => {
				let data = JSON.parse(result);
				console.log("ok create like: " + JSON.stringify(data));
				$(reviewEle).find(".like-act i").addClass("liked");
				$(reviewEle).find(".like-act a").attr("likeId", data.id);
				let likedAmount = parseInt($(reviewEle).find(".liked-amount").text());
				$(reviewEle).find(".liked-amount").text(likedAmount + 1);
			}).fail(error => {
				console.log("error create like: " + JSON.stringify(error));
				if (error.status == 401) {
					window.location.href = "/signin";
				}
			});
		}

	});
}

function createSymptomEle(symptomData) {
	let ele = $("#symptoms-id .hide").clone().removeClass("hide");
	$(ele).attr("id", `symptom-id-${symptomData.id}`);
	$(ele).find(".symptom-name").text(symptomData.symptom.symptomName);
	$(ele).find(".symptom-desc").text(symptomData.symptomValueDesc);
	return ele;
}

function getCommentClone() {
	return $("#comment-list #comment-id-x.hide").clone().removeClass("hide");
}

function starHoverListenner() {
	$("#reviewModal .modal-body .star-mark").hover(function () {
		let hovered_num = getStarNum(this);
		setScoreText(starAndText[hovered_num]);

		$("#reviewModal .modal-body .star-mark").each(function () {
			if (getStarNum($(this)) <= hovered_num) {
				setStarClass("1", this);
			} else {
				setStarClass("0", this);
			}
		});
	}, function () {
		$("#reviewModal .modal-body .fa-star").each(function () {
			if (starSelecting == null) {
				setScoreText("");
				setStarClass("0", this);
			} else {
				setScoreText(starAndText[starSelecting]);
				if (getStarNum(this) <= starSelecting) {
					setStarClass("1", this);
				} else {
					setStarClass("0", this);
				}
			}
		});

	});
}

function getStarNum(ele) {
	return parseInt($(ele).attr("starnum"));
}

function setScoreText(text) {
	$("#reviewModal .modal-body .score-text").text(text);
}

function generateDiaseaseApiUrl(diaseaseId) {
	return `/api/diaseases/${diaseaseId}`;
}

function generatePopularSymptomsApiUrl(diaseaseId) {
	return `/api/getPopularSymptomValue/${diaseaseId}`;
}

function generateReviewsApiUrl(diaseaseId) {
	return `/api/diaseases/${diaseaseId}/reviews`;
}

function getDiaseaseIdFromUrl() {
	let urlPath = window.location.pathname;
	return urlPath.split("/")[2];
}

function getDateFromStr(str) {
    let date = new Date(str);
    let result =  date.getUTCDate() + "/" + (date.getUTCMonth() + 1) + "/" + date.getUTCFullYear();
    console.log("date: " + result); 
    return result;
}