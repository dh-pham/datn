$(document).ready(function () {
    $("a.dropdown-item").hover(function () {
        $(this).addClass("active");
    }, function () {
        $(this).removeClass("active");
    });
    if (sessionStorage.getItem("profilePic") != null) setUserInfo();

    if ($("img.profile-pic").length == 1 && $("img.profile-pic").attr("src") == "#") {
        $.ajax({
            type: "GET",
            url: "/api/getMyInfo"
        }).done(function (data) {
            console.log(data);
            if (typeof (Storage) !== "undefined") {
                // Code for localStorage/sessionStorage.
                setUserSessionStorage(data);
                setUserInfo();
            } else {
                alert("Phiên bản trình duyệt của bạn không hỗ trợ sessionStorage! Nội dung hiển thị có thể sẽ không đúng!");
            }
        }).fail(function (error) {
            console.log(error);
        });
    }
    setLogoutListen();
    setSearchListen();
});

function setSearchListen() {
    $("#nav-search-btn").on("click", function(e) {
		e.preventDefault();
		goSearching();
	});
	
	$("#text-input").keypress(function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			goSearching();
		}
	});
}
function setUserInfo() {
    $("img.profile-pic").attr("src", sessionStorage.getItem("profilePic"));
    $(".profile-dropdown .username").text(sessionStorage.getItem("userName"));
}

function setUserSessionStorage(data) {
    sessionStorage.setItem("userName", data.name);
    sessionStorage.setItem("profilePic", data.image);
    sessionStorage.setItem("email", data.email);
    sessionStorage.setItem("startedAt", data.startedAt);
    sessionStorage.setItem("userId", data.id);
}

function deleteUserSessionStorage() {
    sessionStorage.removeItem("userName");
    sessionStorage.removeItem("profilePic");
    sessionStorage.removeItem("email");
    sessionStorage.removeItem("startedAt");
    sessionStorage.removeItem("userId");
}

function setLogoutListen() {
    $("a.logout-link").click(function(e) {  
        deleteUserSessionStorage();
        window.location.href = "/perform_logout";
    });
}

function getKeywordFromTextBox() {
	let keyword = $("#text-input").val().trim();
	console.log("text-input: " + keyword);
	if (keyword == "") return null;
	return keyword;
}

function goSearching() {
	let keyword = getKeywordFromTextBox();
	window.location.href = generatePageUrl(keyword, 0);
}

function generatePageUrl(keyword, pageIndex) {
	if (pageIndex == null) pageIndex = 0;
	if (keyword == null) {
		return `/diaseases?page=${pageIndex}`;
	} else {
		return `/diaseases?q=${keyword}&page=${pageIndex}`;
	}
}